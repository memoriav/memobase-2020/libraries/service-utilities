## Service Utilities

Releases: [Package Registry](https://gitlab.switch.ch/memoriav/memobase-2020/libraries/package-registry/-/packages)

A library with utility classes, functions and constants used in the import process services.