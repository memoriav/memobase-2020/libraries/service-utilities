/*
   Copyright 2020 Jonas Waeber

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package ch.memobase.test

import ch.memobase.settings.InvalidSettingsValue
import ch.memobase.settings.SettingsLoader
import ch.memobase.settings.SftpSettings
import org.apache.logging.log4j.LogManager
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Condition
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestSettingsLoader {
    private val log = LogManager.getLogger(this::class.java)

    @ParameterizedTest
    @MethodSource("settingsLoaderTestParams")
    fun `test settings loader`(params: SettingsLoaderTestParams) {
        val settings = SettingsLoader(
            params.registeredAppSettings,
            params.fileName,
            params.useKafkaProducerConfig,
            params.useKafkaStreamsConfig,
            params.useKafkaConsumerConfig,
            params.readSftpSettings
        )

        if (params.useKafkaStreamsConfig) {
            assertAll(
                {
                    assertThat(settings.kafkaStreamsSettings)
                        .isNotNull
                },
                {
                    assertThat(settings.kafkaStreamsSettings.getProperty("ssl.keystore.certificate.chain"))
                        .isNotNull()
                        .isNotEmpty()
                },
                {
                    assertThat(settings.kafkaStreamsSettings.getProperty("ssl.keystore.key"))
                        .isNotNull()
                        .isNotEmpty()
                },
                {
                    assertThat(settings.kafkaStreamsSettings.getProperty("ssl.truststore.certificates"))
                        .isNotNull()
                        .isNotEmpty()
                }
            )
        }


        assertThat(settings)
            .isNotNull
            .hasFieldOrProperty("appSettings")
            .satisfies(
                Condition(
                    {
                        params.registeredAppSettings.all {
                            settings.appSettings.getProperty(it).isNotEmpty()
                        }
                    }, "all app settings are not empty!"
                )
            )
    }

    @Test
    fun `test valid sftp settings`() {
        val settings = SettingsLoader(
            listOf(),
            "test3.yml",
            readSftpSettings = true
        )
        assertThat(settings.sftpSettings)
            .isEqualTo(
                SftpSettings("localhost", 22000, "user", "password")
            )
    }

    @Test
    fun `test invalid port value`() {
        Assertions.assertThatExceptionOfType(InvalidSettingsValue::class.java)
            .isThrownBy {
                SettingsLoader(listOf(), "test4.yml", readSftpSettings = true)
            }
    }

    private fun settingsLoaderTestParams() = Stream.of(
        SettingsLoaderTestParams(
            listOf("directory"),
            "test1.yml",
            useKafkaConsumerConfig = false,
            useKafkaStreamsConfig = false,
            useKafkaProducerConfig = false,
            readSftpSettings = false
        ),
        SettingsLoaderTestParams(
            listOf(
                "sheet",
                "header.count",
                "header.line",
                "identifier"
            ),
            "test2.yml",
            useKafkaConsumerConfig = false,
            useKafkaStreamsConfig = true,
            useKafkaProducerConfig = false,
            readSftpSettings = false
        ),
        SettingsLoaderTestParams(
            listOf(
                "elastic.index",
                "elastic.port",
                "elastic.host"
            ),
            "test6.yml",
            useKafkaConsumerConfig = true,
            useKafkaStreamsConfig = false,
            useKafkaProducerConfig = false,
            readSftpSettings = false
        )
    )
}
