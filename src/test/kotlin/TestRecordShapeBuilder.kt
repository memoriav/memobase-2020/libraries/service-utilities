/*
   Copyright 2020 Jonas Waeber

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package ch.memobase.test

import ch.memobase.rdf.validation.RecordShapeBuilder
import ch.memobase.rdf.validation.ShapeModel
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFDataMgr
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.io.path.Path
import kotlin.io.path.exists

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestRecordShapeBuilder {

    private fun runTest(shapeModel: ShapeModel) {
        val model = ModelFactory.createDefaultModel()
        RDFDataMgr.read(model, "src/test/resources/shapes/input/example.nt", Lang.NT)

        val test = File("src/test/resources/jsonld/item.json").inputStream()
        val model2 = ModelFactory.createDefaultModel()
        RDFDataMgr.read(model2, test, Lang.JSONLD11)

        val result = shapeModel.validate(model)
        val result2 = shapeModel.validate(model2)

        println(result.second)
        println(result2.second)

        assertAll(
            { assertTrue(result.first, "validation should be successful") },
            { assertTrue(result2.first, "validation should be successful") },
        )
    }

    @Test
    fun `test single entity`() {
        val test = File("src/test/resources/jsonld/single.json").inputStream()
        val model2 = ModelFactory.createDefaultModel()
        RDFDataMgr.read(model2, test, Lang.JSONLD11)


        val shapeBuilder = RecordShapeBuilder()
        val result = shapeBuilder.allShapes().validate(model2)

        println(result.second)

        assertAll(
            { assertTrue(result.first, "validation should be successful") },
        )
    }

    @Test
    @Disabled
    fun `test single entity specific`() {
        val test = File("/home/jonas/Documents/data/memobase/api/test/mbr:bar-001-CGS_0285-3_d.json").inputStream()
        val model2 = ModelFactory.createDefaultModel()
        RDFDataMgr.read(model2, test, Lang.JSONLD11)


        val shapeBuilder = RecordShapeBuilder()
        val result = shapeBuilder.allShapes().validate(model2)

        println(result.second)

        assertAll(
            { assertTrue(result.first, "validation should be successful") },
        )
    }

    @Test
    @Disabled
    fun `test all`() {
        var count = 1

        val f = File("/home/jonas/Documents/data/memobase/api/reports")

        f.deleteRecursively()
        Files.createDirectory(f.toPath())

        val results = Files.walk(Path("/home/jonas/Documents/data/memobase/api/test"))
            .map { path ->
                path.toFile()
            }
            .filter { file ->
                file.isFile
            }
            .map { file ->
                Pair(file.inputStream(), file.nameWithoutExtension)
            }.map { content ->
                val model = ModelFactory.createDefaultModel()
                RDFDataMgr.read(model, content.first, Lang.JSONLD11)
                content.first.close()
                val parts = content.second.substringAfter(":").split("-")
                Triple(model, content.second, parts[0] + "-" + parts[1])
            }.map { model ->
                val shapeBuilder = RecordShapeBuilder()
                Triple(shapeBuilder.allShapes().validate(model.first), model.second, model.third)
            }.map { report ->
                if (!report.first.first) {
                    val directory = Paths.get("/home/jonas/Documents/data/memobase/api/reports", report.third)
                    if (!directory.exists())
                        Files.createDirectory(directory)

                    Paths.get("/home/jonas/Documents/data/memobase/api/reports", report.third, "${report.second}.ttl").toFile().outputStream().use { output ->
                        output.bufferedWriter(Charsets.UTF_8).use { writer ->
                            writer.write(report.first.second)
                        }
                    }
                }
                if (count % 100 == 0) {
                    println("Processed $count files.")
                }
                count += 1
                report.first.first
            }

        var failures = 1
        for (result in results) {
            if (!result)
                failures += 1
        }
        if (failures > 0) {
            println("Failures: $failures/$count.")
            assertTrue(false)
        } else {
            assertTrue(true)
        }
    }


    @Test
    fun `test activity shape`() {
        val shapeBuilder = RecordShapeBuilder()

        val shapeModel = shapeBuilder.activityShape()
        shapeModel.writeModelToFile("src/test/resources/shapes", "activity_shape")

        runTest(shapeModel)
    }


    @Test
    fun `test mechanism shape`() {
        val shapeBuilder = RecordShapeBuilder()

        val shapeModel = shapeBuilder.mechanismShape(
           shapeBuilder.mechanismNames
        )
        runTest(shapeModel)
    }


    @Test
    fun `test carrier type shape`() {
        val shapeBuilder = RecordShapeBuilder()

        val shapeModel = shapeBuilder.carrierTypeShape()

        shapeModel.writeModelToFile("src/test/resources/shapes", "carrier_type_shape")
        runTest(shapeModel)
    }

    @Test
    fun `test record shape`() {
        val shapeBuilder = RecordShapeBuilder()
        val shapeModel = shapeBuilder.recordShape()
        shapeModel.writeModelToFile("src/test/resources/shapes", "record_shape")
        runTest(shapeModel)
    }

    @Test
    fun `test instantiation shape`() {
        val shapeBuilder = RecordShapeBuilder()
        val shapeModel = shapeBuilder.instantiationShape()
        shapeModel.writeModelToFile("src/test/resources/shapes", "instantiation_shape")
        runTest(shapeModel)
    }

    @Test
    fun `test agent shape`() {
        val shapeBuilder = RecordShapeBuilder()
        val shapeModel = shapeBuilder.agentShape()
        shapeModel.writeModelToFile("src/test/resources/shapes", "agent_shape")
        runTest(shapeModel)
    }

    @Test
    fun `test person shape`() {
        val shapeBuilder = RecordShapeBuilder()
        val shapeModel = shapeBuilder.personShape()
        shapeModel.writeModelToFile("src/test/resources/shapes", "person_shape")
        runTest(shapeModel)
    }

    @Test
    fun `test corporate body shape`() {
        val shapeBuilder = RecordShapeBuilder()
        val shapeModel = shapeBuilder.corporateBodyShape()
        shapeModel.writeModelToFile("src/test/resources/shapes", "corporate_body_shape")
        runTest(shapeModel)
    }

    @Test
    fun `test single date shape`() {
        val shapeBuilder = RecordShapeBuilder()
        val shapeModel = shapeBuilder.singleDate()
        shapeModel.writeModelToFile("src/test/resources/shapes", "single_date_shape")
        runTest(shapeModel)
    }

    @Test
    fun `test date range shape`() {
        val shapeBuilder = RecordShapeBuilder()
        val shapeModel = shapeBuilder.dateRange()
        shapeModel.writeModelToFile("src/test/resources/shapes", "date_range_shape")
        runTest(shapeModel)
    }

    @Test
    fun `test place shape`() {
        val shapeBuilder = RecordShapeBuilder()
        val shapeModel = shapeBuilder.placeShape()
        shapeModel.writeModelToFile("src/test/resources/shapes", "place_shape")
        runTest(shapeModel)
    }

    @Test
    fun `test has place of capture place object shape`() {
        val shapeBuilder = RecordShapeBuilder()
        val shapeModel = shapeBuilder.hasPlaceOfCaptureObjectProperty()
        shapeModel.writeModelToFile("src/test/resources/shapes", "has_place_of_capture_object")
        runTest(shapeModel)
    }

    @Test
    fun `test language shape`() {
        val shapeBuilder = RecordShapeBuilder()
        val shapeModel = shapeBuilder.languageShape()
        shapeModel.writeModelToFile("src/test/resources/shapes", "language_shape")
        runTest(shapeModel)
    }

    @Test
    fun `test title shape`() {
        val shapeBuilder = RecordShapeBuilder()
        val shapeModel = shapeBuilder.titleShape()
        shapeModel.writeModelToFile("src/test/resources/shapes", "title_shape")
        runTest(shapeModel)
    }

    @Test
    fun `test identifier shape`() {
        val shapeBuilder = RecordShapeBuilder()
        val shapeModel = shapeBuilder.identifierShape()
        shapeModel.writeModelToFile("src/test/resources/shapes", "identifier_shape")
        runTest(shapeModel)
    }

    @Test
    fun `test creation relation shape`() {
        val shapeBuilder = RecordShapeBuilder()
        val shapeModel = shapeBuilder.creationRelationShape()
        shapeModel.writeModelToFile("src/test/resources/shapes", "creation_relation_shape")
        runTest(shapeModel)
    }

    @Test
    fun `test record resource holding relation shape`() {
        val shapeBuilder = RecordShapeBuilder()
        val shapeModel = shapeBuilder.recordResourceHoldingRelationShape()
        shapeModel.writeModelToFile("src/test/resources/shapes", "record_resource_holding_relation_shape")
        runTest(shapeModel)
    }

    @Test
    fun `test rule shape`() {
        val shapeBuilder = RecordShapeBuilder()
        val shapeModel = shapeBuilder.ruleShape()
        shapeModel.writeModelToFile("src/test/resources/shapes", "rule_shape")
        runTest(shapeModel)
    }

    @Test
    fun `test skos concept shape`() {
        val shapeBuilder = RecordShapeBuilder()
        val shapeModel = shapeBuilder.skosConceptShape()
        shapeModel.writeModelToFile("src/test/resources/shapes", "skos_concept_shape")
        runTest(shapeModel)
    }
}