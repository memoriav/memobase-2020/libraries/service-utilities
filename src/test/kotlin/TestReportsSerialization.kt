/*
   Copyright 2020 - present Jonas Waeber

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package ch.memobase.test

import ch.memobase.reporting.Report
import ch.memobase.reporting.ReportStatus
import kotlinx.serialization.json.Json
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestReportsSerialization {


    @Test
    fun `test reports serialization`() {
        val report = Report(
            id = "test",
            status = ReportStatus.fatal,
            message = "test",
            step = "test",
            stepVersion = "1.0.0"
        )

        val output = report.toJson()
        assertAll(
            {
                assertThat(output)
                    .isNotNull
                    .isNotEmpty()
            },
            {
                assertThat(output)
                    .contains("id")
                    .contains("status")
                    .contains("message")
                    .contains("step")
                    .contains("timestamp")
                    .contains("stepVersion")
            },
            {
                assertThat(output)
                    .doesNotContain("content")
            }
        )
    }

    @Test
    fun `test report serialization with content`() {
        val report = Report(
            id = "test",
            status = ReportStatus.fatal,
            message = "test",
            step = "test",
            content = "test",
            stepVersion = "1.0.0"
        )

        val output = report.toJson()
        assertAll(
            {
                assertThat(output)
                    .isNotNull
                    .isNotEmpty()
            },
            {
                assertThat(output)
                    .contains("id")
                    .contains("status")
                    .contains("message")
                    .contains("step")
                    .contains("timestamp")
                    .contains("content")
                    .contains("stepVersion")
            }
        )
    }


    @Test
    fun `test reports deserialization`() {
        val report = Report(
            id = "test",
            status = ReportStatus.fatal,
            message = "test",
            step = "test",
            stepVersion = "1.0.0",
        )

        val output = report.toJson()
        val deserializedReport = Json.decodeFromString(Report.serializer(), output)

        assertAll(
            {
                assertThat(deserializedReport)
                    .isNotNull
            },
            {
                assertThat(deserializedReport)
                    .isEqualTo(report)
            },
            {
                assertThat(deserializedReport.content)
                    .isNull()
            }
        )
    }
    @Test
    fun `test report deserialization with content`() {
        val report = Report(
            id = "test",
            status = ReportStatus.fatal,
            message = "test",
            step = "test",
            content = "test",
            stepVersion = "1.0.0",
        )

        val output = report.toJson()
        val deserializedReport = Json.decodeFromString(Report.serializer(), output)

        assertAll(
            {
                assertThat(deserializedReport)
                    .isNotNull
            },
            {
                assertThat(deserializedReport)
                    .isEqualTo(report)
            },
            {
                assertThat(deserializedReport.content)
                    .isEqualTo(report.content)
            }
        )
    }
}