/*
   Copyright 2020 - present Jonas Waeber

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package ch.memobase.test

import ch.memobase.settings.HeaderExtractionSupplier
import ch.memobase.settings.HeaderMetadata
import kotlinx.serialization.json.Json
import org.apache.kafka.common.header.internals.RecordHeader
import org.apache.kafka.common.header.internals.RecordHeaders
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.StreamsConfig
import org.apache.kafka.streams.TopologyTestDriver
import org.apache.kafka.streams.test.TestRecord
import org.apache.logging.log4j.LogManager
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import java.util.*


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestHeaderExtraction {

    private val log = LogManager.getLogger("TestLogger")

    @Test
    fun `test header extraction`() {
        val stream = StreamsBuilder()
        stream
            .stream<String, String>("newTopic")
            .map { key, value ->
                log.debug("Processing key: $key, value: $value")
                KeyValue(key, value)
            }
            .processValues(HeaderExtractionSupplier<String>())
            .map { key, value ->
                KeyValue(key, Json.encodeToString(HeaderMetadata.serializer(), value.second))
            }
            .to("test-output")
        val topology = stream.build()

        val props = Properties()
        props.setProperty(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().javaClass.name)
        props.setProperty(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().javaClass.name)
        val driver = TopologyTestDriver(topology, props)

        val headers = RecordHeaders()
        headers.add(RecordHeader("sessionId", "test-session-id".toByteArray()))
        headers.add(RecordHeader("recordSetId", "test-record-set-id".toByteArray()))
        headers.add(RecordHeader("institutionId", "test-institution-id".toByteArray()))
        headers.add(RecordHeader("isPublished", "false".toByteArray()))
        headers.add(RecordHeader("xmlRecordTag", "record".toByteArray()))
        headers.add(RecordHeader("xmlIdentifierFieldName", "id".toByteArray()))
        headers.add(RecordHeader("tableSheetIndex", "1".toByteArray()))
        headers.add(RecordHeader("tableHeaderCount", "1".toByteArray()))
        headers.add(RecordHeader("tableHeaderIndex", "1".toByteArray()))
        headers.add(RecordHeader("tableIdentifierIndex", "1".toByteArray()))

        val inputTopic = driver.createInputTopic("newTopic", Serdes.String().serializer(), Serdes.String().serializer())
        inputTopic.pipeInput(TestRecord("test-key", "message content", headers))


        val outputTopic =
            driver.createOutputTopic("test-output", Serdes.String().deserializer(), Serdes.String().deserializer())
        val output = outputTopic.readRecord()

        assertAll(
            {
                assertThat(output.value())
                    .isEqualTo("{\"recordSetId\":\"test-record-set-id\",\"sessionId\":\"test-session-id\",\"institutionId\":\"test-institution-id\",\"isPublished\":false,\"xmlRecordTag\":\"record\",\"xmlIdentifierFieldName\":\"id\",\"tableSheetIndex\":1,\"tableHeaderCount\":1,\"tableHeaderIndex\":1,\"tableIdentifierIndex\":1}")
            },
            {
                assertThat(output.key())
                    .isEqualTo("test-key")
            }
        )
    }
}
