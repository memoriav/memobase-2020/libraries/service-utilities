/*
   Copyright 2020 - present Jonas Waeber

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package ch.memobase.test

import ch.memobase.settings.SftpSettings
import ch.memobase.sftp.SftpClient
import ch.memobase.testing.EmbeddedSftpServer
import java.io.File
import java.io.FileInputStream
import java.nio.file.Paths
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestSftpClient {

    private val sftpServer = EmbeddedSftpServer(22000, "user", "password")
    private val sftpClient = SftpClient(SftpSettings("localhost", 22000, "user", "password"))

    init {
        val files = listOf(
                Pair("/memobase/test_record_set_1", "file1.txt"),
                Pair("/memobase/test_record_set_2", "file1.txt"),
                Pair("/memobase/test_record_set_2", "file2.txt"),
                Pair("/memobase/test_record_set_2", "file3.txt"),
                Pair("/memobase/test_record_set_3", "file4.txt")
        )

        for (pair in files) {
            sftpServer.putFile(Paths.get(pair.first, pair.second).toString(), FileInputStream(Paths.get("src/test/resources/data", pair.second).toFile()))
        }
    }

    @Test
    fun `test sftp client list files`() {
        val files = sftpClient.listFiles("/memobase/test_record_set_1")
        assertThat(files)
                .hasSize(1)
    }

    @Test
    fun `test sftp client list several files`() {
        val files = sftpClient.listFiles("/memobase/test_record_set_2")
        assertThat(files)
                .hasSize(3)
    }

    @Test
    fun `test sftp client puts file`() {
        sftpClient.put("src/test/resources/data/file1.txt", "/memobase/put_file1.txt")

        assertThat(sftpServer.existsFile("/memobase/put_file1.txt")).isTrue()
    }

    @ExperimentalStdlibApi
    @Test
    fun `test sftp client open files`() {
        val files = sftpClient.listFiles("/memobase/test_record_set_2")

        for (file in files) {
            val stream = sftpClient.open(File(file))
            stream.use {
                assertThat(stream.bufferedReader().readText())
                        .isEqualTo("test")
            }
        }
    }

    @Test
    fun `test sftp client file exists`() {
        val result = sftpClient.exists("/memobase/test_record_set_1/file1.txt")

        assertThat(result)
                .isTrue()
    }

    @Test
    fun `test sftp client file does not exists`() {
        val result = sftpClient.exists("/memobase/test_record_set_1/file2.txt")

        assertThat(result)
                .isFalse()
    }
}
