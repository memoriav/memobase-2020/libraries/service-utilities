package ch.memobase.model.input

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RdfResource(
    @SerialName("@id")
    val id: String
)