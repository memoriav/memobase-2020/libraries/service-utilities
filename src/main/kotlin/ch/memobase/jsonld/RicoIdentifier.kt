package ch.memobase.model.input

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RicoIdentifier(
    @SerialName("@id")
    val id: String,
    @SerialName("@type")
    val rdfType: String,
    val identifier: String,
    val type: String,
)
