package ch.memobase.model.input

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RicoMechanism(
    @SerialName("@id")
    val id: String,
    @SerialName("@type")
    val rdfType: String,


    val name: String,
    val performsOrPerformed: RdfResource,

    )