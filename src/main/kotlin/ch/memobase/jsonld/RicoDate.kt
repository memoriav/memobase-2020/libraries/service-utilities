package ch.memobase.model.input

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RicoDate(
    @SerialName("@id")
    val id: String? = null,
    @SerialName("@type")
    val rdfType: String,
    val certainty: String? = null,
    val dateQualifier: String? = null,
    val expressedDate: String? = null,
    val normalizedDateValue: String? = null,
)

