package ch.memobase.model.input

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SkosConcept(
    @SerialName("@id")
    val id: String,
    @SerialName("@type")
    val rdfType: String,


    val prefLabel: String?,
    val prefLabelDe: String?,
    val prefLabelFr: String?,
    val prefLabelIt: String?,


    val isOrWasAffectedBy: RicoActivity? = null,

    )