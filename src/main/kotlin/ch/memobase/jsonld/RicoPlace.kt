package ch.memobase.model.input

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RicoPlace(
    @SerialName("@id")
    val id: String,
    @SerialName("@type")
    val rdfType: String,


    @SerialName("name_de")
    val nameDe: String? = null,
    @SerialName("name_fr")
    val nameFr: String? = null,
    @SerialName("name_it")
    val nameIt: String? = null,

    @SerialName("descriptiveNote_de")
    val descriptiveNodeDe: String,
    @SerialName("descriptiveNote_fr")
    val descriptiveNodeFr: String,
    @SerialName("descriptiveNote_it")
    val descriptiveNodeIt: String,
    val sameAs: String,
    val resultsOrResultedFrom: RicoActivity
)