/*
   Copyright 2020 Jonas Waeber

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package ch.memobase.rdf

import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.ResourceFactory

object EBUCORE {

    val hasGenre = prop("hasGenre")
    val hasFormat = prop("hasFormat")
    val hasMedium = prop("hasMedium")
    val duration = prop("duration")
    val displayAspectRatio = prop("displayAspectRatio")
    val audioTrackConfiguration = prop("audioTrackConfiguration")
    val playbackSpeed = prop("playbackSpeed")
    val hasStandard = prop("hasStandard")
    val locator = prop("locator")
    val hasMimeType = prop("hasMimeType")
    val height = prop("height")
    val width = prop("width")
    val orientation = prop("orientation")
    val mediaResourceDescription = prop("mediaResourceDescription")
    val fileSize = prop("fileSize")
    val isDistributedOn = prop("isDistributedOn")

    private fun prop(name: String): Property {
        return ResourceFactory.createProperty(NS.ebucore, name)
    }

    private fun res(name: String): Resource {
        return ResourceFactory.createResource(NS.ebucore + name)
    }
}
