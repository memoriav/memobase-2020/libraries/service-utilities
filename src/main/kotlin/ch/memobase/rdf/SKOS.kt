/*
   Copyright 2020 Jonas Waeber

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package ch.memobase.rdf

import ch.memobase.exceptions.InvalidMappingException
import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.ResourceFactory

object SKOS {

    val labelProperties = listOf("prefLabel")

    val acceptedPropertiesList = listOf("prefLabel", "altLabel", "hiddenLabel",
        "changeNote", "definition", "editorialNote", "example", "historyNote", "notation", "note", "scopeNote")

    fun get(name: String): Property {
        if (acceptedPropertiesList.contains(name)) {
            return prop(name)
        } else {
            throw InvalidMappingException(
                "Could not load skos property '$name' as it " +
                        "is not on the list of accepted properties: $acceptedPropertiesList."
            )
        }
    }

    // Classes

    val ConceptScheme: Resource = res("ConceptScheme")
    val Collection: Resource = res("Collection")
    val OrderedCollection: Resource = res("OrderedCollection")
    val Concept: Resource = res("Concept")

    // Labels

    val prefLabel: Property = prop("prefLabel")
    val altLabel: Property = prop("altLabel")
    val hiddenLabel: Property = prop("hiddenLabel")

    // Annotation Properties

    val changeNote: Property = prop("changeNote")
    val definition: Property = prop("definition")
    val editorialNote: Property = prop("editorialNote")
    val example: Property = prop("example")
    val historyNote: Property = prop("historyNote")
    val notation: Property = prop("notation")
    val note: Property = prop("note")
    val scopeNote: Property = prop("scopeNote")

    // Mapping Properties

    val mappingRelation: Property = prop("mappingRelation")
    val exactMatch: Property = prop("exactMatch")
    val closeMatch: Property = prop("closeMatch")
    val narrowMatch: Property = prop("narrowMatch")
    val broadMatch: Property = prop("broadMatch")

    // Semantic Relation Properties

    val semanticRelation: Property = prop("semanticRelation")
    val related: Property = prop("related")
    val relatedMatch: Property = prop("relatedMatch")

    // Hierarchy

    val topConceptOf: Property = prop("topConceptOf")
    val hasTopConcept: Property = prop("hasTopConcept")
    val inScheme: Property = prop("inScheme")
    val broader: Property = prop("broader")
    val broaderTransitive: Property = prop("broaderTransitive")
    val narrower: Property = prop("narrower")
    val narrowerTransitive: Property = prop("narrowerTransitive")

    // Collection Property

    val member: Property = prop("member")
    val memberList: Property = prop("memberList")

    private fun prop(name: String): Property {
        return ResourceFactory.createProperty(NS.skos, name)
    }

    private fun res(name: String): Resource {
        return ResourceFactory.createResource(NS.skos + name)
    }
}
