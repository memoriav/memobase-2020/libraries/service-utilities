/*
   Copyright 2020 Jonas Waeber

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package ch.memobase.rdf

import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.ResourceFactory

object XSD {
    val dateTime: Property = prop("dateTime")
    val date: Property = prop("date")
    val integer: Property = prop("integer")
    val boolean: Property = prop("boolean")
    val string: Property = prop("string")
    val hexBinary: Property = prop("hexBinary")

    private fun prop(name: String): Property {
        return ResourceFactory.createProperty(NS.xsd, name)
    }
}
