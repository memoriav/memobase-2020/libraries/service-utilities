package ch.memobase.rdf

import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.ResourceFactory

object EDM {
    val componentColor = EDM.prop("componentColor")

    private fun prop(name: String): Property {
        return ResourceFactory.createProperty(NS.edm, name)
    }

    private fun res(name: String): Resource {
        return ResourceFactory.createResource(NS.edm + name)
    }
}