/*
   Copyright 2020 Jonas Waeber

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package ch.memobase.rdf

import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.ResourceFactory

/**
 * RDA elements sets: Unconstrained properties
 *
 * [Link](https://www.rdaregistry.info/Elements/u/)
 *
 * Properties that are derived from the elements of all of the RDA entities with semantics that are
 * independent of the IFLA Library Reference Model.
 */
object RDA {
    /**
     * has sponsoring agent of resource (rdau:P60451): Relates a resource to an agent who sponsors some aspect
     * of a resource.
     */
    val hasSponsoringAgentOfResource = prop("P60451")

    /**
     * has sponsored resource of agent (): Relates an agent to a resource that involves a responsibility
     * of an agent for sponsoring some aspect of a resource.
     */
    val hasSponsoredResourceOfAgent = prop("P60675")

    /**
     * has finding aid (rdau:P60262): Relates a resource to a resource that is a metadata description set for a
     * collection resource.
     */
    val hasFindingAid = prop("P60262")

    /**
     * has place of capture (rdau:P60556): Relates a resource to a place associated with recording, filming, etc.,
     * the content of a resource.
     */
    val hasPlaceOfCapture = prop("P60556")

    /**
     * is place of capture of (rdau:P60966): Relates a place to a resource that has content recorded, filmed, etc. in a place.
     */
    val isPlaceOfCaptureOf = prop("P60966")

    /**
     * has colour content (rdau:P60558): Relates a resource to an indication of the presence of colour or
     * tone in a resource.
     */
    val hasColourContent = prop("P60558")

    /**
     * has producer (rdau:P60441): Relates a resource to an agent who is responsible for inscribing, fabricating,
     * constructing, or other method of producing, an unpublished resource.
     */
    val hasProducer = prop("P60441")

    /**
     * is producer of (rdau:P60668) Relates an agent to a resource that involves a responsibility of an agent for most of the business aspects
     *  of a production for screen, sound recording, television, webcast, etc.
     */
    val isProducerOf = prop("P60668")

    /**
     * has language of resource (rdau:P60099): Relates a resource to a language used for the content of a resource.
     */
    val hasLanguageOfResource = prop("P60099")

    /**
     * has restriction on access (rdau:P60496): Relates a resource to limitations placed on access to a resource.
     */
    val hasRestrictionOnAccess = prop("P60496")

    /**
     * has profession or occupation (rdau:P60468): Relates an agent to an agent's vocation or avocation.
     */
    val hasProfessionOrOccupation = prop("P60468")

    /**
     * has variant name of agent (rdau:P60119): Relates an agent to a name of agent that is not selected for
     * preference in a specific application or context.
     */
    val hasVariantNameOfAgent = prop("P60119")

    /**
     * is member of (rdau:P60648): Relates an agent of which an agent is a member to an agent.
     */
    val isMemberOf = prop("P60648")

    /**
     * has period of activity of agent (rdau:P60076): Relates an agent to a date or range of dates indicative of a
     * period in which an agent was active in its primary field of endeavor.
     */
    val hasPeriodOfActivityOfAgent = prop("P60076")

    /**
     * is period of activity of agent of (rdau:P60990): Relates a timespan to an agent who is active in its primary
     * field of endeavour during a timespan.
     */
    val isPeriodOfActivityOfAgentOf = prop("P60990")

    /**
     * has coverage of content (rdau:P60518): Relates a resource to a chronological or geographic coverage of the
     * content of a resource.
     */
    val hasCoverageOfContent = prop("P60518")

    /**
     * has note on resource (rdau:P60470): Relates a resource to a broad unstructured description of one or more
     * attributes of a resource.
     */
    val hasNoteOnResource = prop("P60470")

    private fun prop(name: String): Property {
        return ResourceFactory.createProperty(NS.rdau, name)
    }
}
