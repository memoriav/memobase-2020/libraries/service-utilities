/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.rdf.validation

import ch.memobase.rdf.NS
import org.apache.jena.graph.Graph
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RDFFormat
import org.apache.jena.shacl.ShaclValidator
import org.apache.jena.shacl.Shapes
import java.io.FileOutputStream
import java.io.StringWriter

class ShapeModel(private val prefix: String, private val outputFormat: RDFFormat = RDFFormat.TURTLE) {
    private val model = ModelFactory.createDefaultModel()
    private val validator = ShaclValidator.get()
    private val test = "https://test.memobase.ch/"

    init {
        model.setNsPrefixes(NS.prefixMapping)
        model.setNsPrefix("sh", NS.shacl)
        model.setNsPrefix("test", test)
    }

    fun addFocusNode(name: String, closed: Boolean = true): FocusNode {
        return FocusNode(model, "$test$prefix$name", closed)
    }

    fun writeModelToFile(path: String = "", fileName: String = "shape") {
        val file = if (path != "") "$path/$fileName.ttl" else "$fileName.ttl"
        RDFDataMgr.write(FileOutputStream(file), model, outputFormat)
    }

    fun validate(dataModel: Model): Pair<Boolean, String> {
        val dataGraph: Graph = dataModel.graph
        val shapes = Shapes.parse(model.graph)
        val report = validator.validate(shapes, dataGraph)
        StringWriter().use {
            RDFDataMgr.write(it, report.graph, outputFormat)
            return Pair(report.conforms(), it.toString())
        }
    }

    fun add(shapeModel: ShapeModel): ShapeModel {
        this.model.add(shapeModel.model)
        return this
    }
}
