/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.rdf.validation

import ch.memobase.rdf.RDF
import ch.memobase.rdf.SHACL
import ch.memobase.rdf.XSD
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.RDFNode
import org.apache.jena.rdf.model.Resource

class PropertyNode(private val model: Model, subject: Resource, path: Property) {
    private val propertyNode = model.createResource()

    init {
        propertyNode.addProperty(RDF.type, SHACL.PropertyShape)
        propertyNode.addProperty(SHACL.path, path)
        subject.addProperty(SHACL.property, propertyNode)
    }

    fun asRdfNode(): RDFNode {
        return propertyNode
    }

    fun isNodeKind(): PropertyNode {
        propertyNode.addProperty(SHACL.nodeKind, SHACL.NodeKind)
        return this
    }

    fun isIRINodeKind(): PropertyNode {
        propertyNode.addProperty(SHACL.nodeKind, SHACL.IRI)
        return this
    }

    fun isBlankNodeKind(): PropertyNode {
        propertyNode.addProperty(SHACL.nodeKind, SHACL.BlankNode)
        return this
    }

    fun isLiteralNodeKind(datatype: Property): PropertyNode {
        propertyNode.addProperty(SHACL.nodeKind, SHACL.Literal)
        propertyNode.addProperty(SHACL.datatype, datatype)
        return this
    }

    fun isLiteralNodeKind(vararg datatypes: Property): PropertyNode {
        val dataTypeNodes = mutableListOf<RDFNode>()
        for (datatype in datatypes) {
            val blank = model.createResource().apply {
                addProperty(SHACL.datatype, datatype)
            }
            dataTypeNodes.add(blank)
        }
        val list = model.createList(dataTypeNodes.iterator())
        propertyNode.addProperty(SHACL.or, list)
        return this
    }

    fun isIRIOrLiteralNodeKind(): PropertyNode {
        propertyNode.addProperty(SHACL.nodeKind, SHACL.IRIOrLiteral)
        return this
    }

    fun isBlankNodeOrIRINodeKind(): PropertyNode {
        propertyNode.addProperty(SHACL.nodeKind, SHACL.BlankNodeOrIRI)
        return this
    }

    fun isBlankNodeOrLiteralKind(): PropertyNode {
        propertyNode.addProperty(SHACL.nodeKind, SHACL.BlankNodeOrLiteral)
        return this
    }

    fun minCountCardinality(value: Int): PropertyNode {
        propertyNode.addLiteral(SHACL.minCount, model.createTypedLiteral(value, XSD.integer.uri))
        return this
    }

    fun maxCountCardinality(value: Int): PropertyNode {
        propertyNode.addLiteral(SHACL.maxCount, model.createTypedLiteral(value, XSD.integer.uri))
        return this
    }

    fun hasValue(resource: Resource): PropertyNode {
        propertyNode.addProperty(SHACL.hasValue, resource)
        return this
    }

    fun isIn(vararg resource: Resource): PropertyNode {
        val list = model.createList(resource.map { model.asRDFNode(it.asNode()) }.iterator())
        propertyNode.addProperty(SHACL.shIn, list)
        return this
    }

    fun pattern(value: String): PropertyNode {
        propertyNode.addLiteral(SHACL.pattern, value)
        return this
    }

    fun languageIn(vararg value: String): PropertyNode {
        val list = model.createList(value.map { model.asRDFNode(model.createLiteral(it).asNode()) }.iterator())
        propertyNode.addProperty(SHACL.languageIn, list)
        return this
    }

    fun uniqueLang(): PropertyNode {
        propertyNode.addLiteral(SHACL.uniqueLang, true)
        return this
    }


}