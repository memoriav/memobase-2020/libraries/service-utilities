/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.rdf.validation

import ch.memobase.rdf.*
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Resource

class RecordShapeBuilder {

    private fun createSpecificNode(namespace: String, id: String): Resource {
        return ModelFactory.createDefaultModel().createResource(namespace + id)
    }

    val mechanismNames = listOf(
        "Enriched by Connectom",
        "CarrierTypeNormalizer",
        "Wikidata Linker - Place",
        "Wikidata Linker - Agent",
        // TODO: remove this once it is fixed!
        "Wikidata Linker - Person",
        "LanguagesNormalizer",
        "GenreNormalizer",
    )

    fun allShapes(): ShapeModel {
        return activityShape()
            .add(mechanismShape(mechanismNames))
            .add(carrierTypeShape())
            .add(recordShape())
            .add(instantiationShape())
            .add(agentShape())
            .add(personShape())
            .add(corporateBodyShape())
            .add(singleDate())
            .add(dateRange())
            .add(placeShape())
            .add(hasPlaceOfCaptureObjectProperty())
            .add(languageShape())
            .add(titleShape())
            .add(identifierShape())
            .add(creationRelationShape())
            .add(recordResourceHoldingRelationShape())
            .add(ruleShape())
            .add(skosConceptShape())
    }

    fun activityShape(): ShapeModel {
        val model = ShapeModel("Test")

        val focusNode = model.addFocusNode("Activity")
            .isBlankNodeKind()
            .setTargetClass(RICO.Activity)

        focusNode
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern("enrichment")

        focusNode
            .addProperty(RICO.beginningDate)
            .isLiteralNodeKind(XSD.dateTime)
            .maxCountCardinality(1)
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.endDate)
            .isLiteralNodeKind(XSD.dateTime)
            .maxCountCardinality(1)
            .minCountCardinality(1)


        focusNode
            .addProperty(RICO.affectsOrAffected)
            .isBlankNodeOrIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        // In the wikidata linker a single match can generate multiple places!
        focusNode
            .addProperty(RICO.resultsOrResultedIn)
            .isBlankNodeKind()
            // TODO: The max I've seen is 14, but theoretically there is no limit with the number of connections
            // created by Connectom. Max three is currently only applicable to the wiki linker enricher done in Memobase
            // would have to use more complex shapes to ensure the difference, if it is even possible.
            // .maxCountCardinality(3)
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.isOrWasPerformedBy)
            .isBlankNodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        return model
    }

    fun mechanismShape(names: List<String>): ShapeModel {
        val model = ShapeModel("Test")
        val focusNode = model.addFocusNode("Mechanism")
            .isBlankNodeKind()
            .setTargetClass(RICO.Mechanism)

        focusNode
            .addProperty(RICO.name)
            .isLiteralNodeKind(XSD.string)
            .minCountCardinality(1)
            .maxCountCardinality(1)
            .pattern(names.joinToString("|"))

        focusNode
            .addProperty(RICO.performsOrPerformed)
            .isBlankNodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        return model
    }

    fun carrierTypeShape(): ShapeModel {
        val model = ShapeModel("Test")
        val normalizedCarrierTypeNode = model.addFocusNode("NormalizedCarrierType")
            .isBlankNodeKind()
        normalizedCarrierTypeNode
            .addProperty(RICO.name)
            .isLiteralNodeKind(RDF.langString)
            .languageIn("de", "fr", "it")
            .uniqueLang()
            .maxCountCardinality(3)
            .minCountCardinality(3)
        normalizedCarrierTypeNode.addProperty(RICO.resultsOrResultedFrom)
            .isBlankNodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)
        normalizedCarrierTypeNode.addProperty(SCHEMA.sameAs)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern("http://www.wikidata.org/entity/Q\\d{1,10}")


        val originCarrierTypeNode = model.addFocusNode("OriginCarrierType")
            .isBlankNodeKind()
        originCarrierTypeNode
            .addProperty(RICO.isOrWasAffectedBy)
            .isBlankNodeKind()
            // TODO: reduce to 1 once normalization is fixed to only produce one activity for the change.
            .maxCountCardinality(3)
            // TODO: isOrWasAffected by is missing when the value has not been normalized, but this should always be
            // normalized, so why is it not?
            .minCountCardinality(0)
        originCarrierTypeNode
            .addProperty(RICO.name)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)

        model.addFocusNode("CarrierType", false)
            .setTargetClass(RICO.CarrierType)
            .or(
                normalizedCarrierTypeNode,
                originCarrierTypeNode
            )

        return model
    }

    fun recordShape(): ShapeModel {
        val model = ShapeModel("Test")

        val focusNode = model.addFocusNode("Record")
            .setTargetClass(RICO.Record)
            .isIRINodeKind()

        focusNode
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern("Film|Foto|Radio|Ton|Tonbildschau|TV|Video")

        focusNode
            .addProperty(DC.relation)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(DC.abstract)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(RICO.descriptiveNote)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(RICO.title)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            // TODO: Some srf-030 records do not have a main title, which leads to there being no
            //  title data property on the record itself. Could reuse the broadcast or series title instead?
            // .minCountCardinality(1)

        focusNode
            .addProperty(RICO.scopeAndContent)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(RICO.source)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(RDA.hasSponsoringAgentOfResource)
            .isIRINodeKind()
            .isIn(MB.memoriavInstitution)
            .maxCountCardinality(1)
            .minCountCardinality(0)

        focusNode
            .addProperty(RICO.hasPublisher)
            .isBlankNodeKind()

        focusNode
            .addProperty(RDA.hasProducer)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.recordResourceOrInstantiationIsSourceOfCreationRelation)
            .isBlankNodeKind()

        focusNode
            .addProperty(DC.created)
            .isBlankNodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(0)

        focusNode
            .addProperty(DC.issued)
            .isBlankNodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(0)

        focusNode
            .addProperty(DC.temporal)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.hasOrHadIdentifier)
            .isBlankNodeKind()
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.hasOrHadLanguage)
            .isBlankNodeKind()

        focusNode
            .addProperty(DC.spatial)
            .isBlankNodeKind()

        focusNode
            .addProperty(RDA.hasPlaceOfCapture)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.isOrWasRegulatedBy)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.hasOrHadTitle)
            .isBlankNodeKind()
            .minCountCardinality(1)

        focusNode
            .addProperty(EBUCORE.hasGenre)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.hasOrHadSubject)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.isOrWasPartOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.hasInstantiation)
            .isIRINodeKind()
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.hasOrHadHolder)
            .isIRINodeKind()
            .minCountCardinality(1)

        focusNode
            .addProperty(SCHEMA.sameAs)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(RICO.isRecordResourceAssociatedWithRecordResource)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.conditionsOfUse)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(RICO.conditionsOfAccess)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(RICO.isOrWasAffectedBy)
            .isBlankNodeKind()

        focusNode
            .addProperty(MB.isPublished)
            .isLiteralNodeKind(XSD.boolean)
            .maxCountCardinality(1)
            .minCountCardinality(1)

        return model
    }


    fun instantiationShape(): ShapeModel {
        val model = ShapeModel("Test")

        val physicalObject = model.addFocusNode("PhysicalObject")
            .isIRINodeKind()

        physicalObject
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern("physicalObject")

        physicalObject
            .addProperty(RICO.hasCarrierType)
            .isBlankNodeKind()

        physicalObject
            .addProperty(RDA.hasColourContent)
            .isLiteralNodeKind(XSD.string)

        physicalObject
            .addProperty(EBUCORE.duration)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .pattern("\\d{2}:\\d{2}(:\\d{2})?")

        physicalObject
            .addProperty(RICO.hasDerivedInstantiation)
            .isIRINodeKind()
            .maxCountCardinality(1)

        physicalObject
            .addProperty(RICO.isInstantiationOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        physicalObject
            .addProperty(RICO.physicalCharacteristics)
            .isLiteralNodeKind(XSD.string)

        physicalObject
            .addProperty(RICO.descriptiveNote)
            .isLiteralNodeKind(XSD.string)

        // TODO: This should always be 2, but the test record has no access rule. This is required according to the documentation.
        physicalObject
            .addProperty(RICO.isOrWasRegulatedBy)
            .isBlankNodeKind()
            .maxCountCardinality(2)
            .minCountCardinality(1)

        physicalObject
            .addProperty(RICO.conditionsOfUse)
            .isLiteralNodeKind(XSD.string)

        physicalObject
            .addProperty(RICO.hasOrHadIdentifier)
            .isBlankNodeKind()

        val digitalObject = model.addFocusNode("DigitalObject")
            .isIRINodeKind()

        digitalObject
            .addProperty(RICO.hasOrHadIdentifier)
            .isBlankNodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        digitalObject
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .minCountCardinality(1)
            .maxCountCardinality(1)
            .pattern("digitalObject")

        digitalObject
            .addProperty(EBUCORE.locator)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            // TODO: Almost all digital objects have a locator, and it would be nice to enforce this in validation.
            //  Only resources with the proxyType proxydirect and the access rule faro do not have one. It might be
            //  possible to configure the shape in such a way to make sure that is always the case.
            .minCountCardinality(0)

        digitalObject
            .addProperty(RICO.descriptiveNote)
            .isLiteralNodeKind(XSD.string)

        digitalObject
            .addProperty(RICO.isOrWasRegulatedBy)
            .isBlankNodeKind()
            // TODO: should faro and access rule be separated this will have a max of 3!
            .maxCountCardinality(2)
            .minCountCardinality(2)

        digitalObject
            .addProperty(RICO.conditionsOfUse)
            .isLiteralNodeKind(XSD.string)

        digitalObject
            .addProperty(RICO.isDerivedFromInstantiation)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        digitalObject
            .addProperty(RICO.hasDerivedInstantiation)
            .isIRINodeKind()
            .maxCountCardinality(1)
            // TODO: Not all resources have a thumbnail? bsg-001-001454543000003-1
            .minCountCardinality(0)

        digitalObject
            .addProperty(RICO.isInstantiationOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        digitalObject
            .addProperty(EBUCORE.isDistributedOn)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            // TODO: can be missing when there is no locator
            .minCountCardinality(0)
            .pattern("audio|video|image|srfaudio|srfvideo|youtube|vimeo|zem")

        digitalObject
            .addProperty(EBUCORE.duration)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
        // TODO: Duration is given in decimal format for the digital objects. This is likely the output of
        // the image processing software. Could be normalized?
        // .pattern("\\d{2}:\\d{2}:\\d{2}")

        val mimeTypes = listOf(
            "image/jpeg",
            "image/png",
            "audio/mp3",
            "video/mp4",
            "image/jpg",
            "text/html",
            "audio/aac",
            "audio/mpeg",
            "audio/ogg",
            "video/webm",
            "application/octet-stream",
            "video/x-m4v"
        )

        digitalObject
            .addProperty(EBUCORE.hasMimeType)
            .isLiteralNodeKind(XSD.string)
            .pattern(mimeTypes.joinToString("|"))

        val hasFormat = listOf(
            "fmt/11",
            "fmt/12",
            "fmt/13",
            "fmt/41",
            "fmt/42",
            "fmt/43",
            "fmt/44",
            "fmt/97",
            "fmt/102",
            "fmt/134",
            "fmt/199",
            "fmt/203",
            "fmt/471",
            "fmt/645",
            "fmt/935",
            "fmt/1280",
            "fmt/1507",
            "fmt/1812",
            "x-fmt/384",
            "x-fmt/390",
            "x-fmt/391",
            "UNKNOWN",
        )

        digitalObject
            .addProperty(EBUCORE.hasFormat)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .pattern(hasFormat.joinToString("|"))

        digitalObject
            .addProperty(EBUCORE.height)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)

        digitalObject
            .addProperty(EBUCORE.width)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)

        val orientation = listOf(
            "BottomRight",
            "LeftBottom",
            "RightTop",
            "TopLeft",
            "Undefined",
        )

        digitalObject
            .addProperty(EBUCORE.orientation)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .pattern(orientation.joinToString("|"))

        digitalObject
            .addProperty(EBUCORE.mediaResourceDescription)
            .isLiteralNodeKind(XSD.string)

        digitalObject
            .addProperty(RDA.hasColourContent)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)


        digitalObject
            .addProperty(EDM.componentColor)
            .isLiteralNodeKind(XSD.hexBinary)
            .maxCountCardinality(10)
            .pattern("[A-Z0-9]{6}")

        digitalObject
            .addProperty(MB.proxyType)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern("proxy|proxydirect")

        digitalObject
            .addProperty(MB.mediaLocation)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            // TODO: digital objects without a locator don't have a media location either. Maybe could do another or?
            .minCountCardinality(0)
            .pattern("remote|local")

        val thumbnail = model.addFocusNode("Thumbnail")
            .isIRINodeKind()

        thumbnail
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern("thumbnail")

        thumbnail
            .addProperty(RICO.hasOrHadIdentifier)
            .isBlankNodeKind()
            .maxCountCardinality(1)
            // TODO: not all thumbnails have an identifier defined!
            .minCountCardinality(0)

        thumbnail
            .addProperty(EBUCORE.locator)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)

        thumbnail
            .addProperty(RICO.isDerivedFromInstantiation)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        thumbnail
            .addProperty(RICO.isInstantiationOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        thumbnail
            .addProperty(EBUCORE.isDistributedOn)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern("image")

        thumbnail
            .addProperty(EBUCORE.hasMimeType)
            .isLiteralNodeKind(XSD.string)
            .pattern(mimeTypes.joinToString("|"))

        thumbnail
            .addProperty(EBUCORE.hasFormat)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .pattern(hasFormat.joinToString("|"))

        thumbnail
            .addProperty(EBUCORE.height)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)

        thumbnail
            .addProperty(EBUCORE.width)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)

        thumbnail
            .addProperty(EBUCORE.orientation)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .pattern(orientation.joinToString("|"))

        thumbnail
            .addProperty(EBUCORE.mediaResourceDescription)
            .isLiteralNodeKind(XSD.string)

        thumbnail
            .addProperty(RDA.hasColourContent)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)

        thumbnail
            .addProperty(EDM.componentColor)
            .isLiteralNodeKind(XSD.hexBinary)
            .maxCountCardinality(10)
            .pattern("[A-Z0-9]{6}")

        model.addFocusNode("Instantiation", false)
            .setTargetClass(RICO.Instantiation)
            .or(
                physicalObject,
                digitalObject,
                thumbnail,
            )

        return model
    }

    fun agentShape(): ShapeModel {
        val model = ShapeModel("Test")

        val focusNode = model.addFocusNode("Agent")
            .setTargetClass(RICO.Agent)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.name)
            .isLiteralNodeKind(XSD.string)
            .minCountCardinality(1)

        focusNode
            .addProperty(SCHEMA.sameAs)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(RDA.hasVariantNameOfAgent)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(RICO.history)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(RICO.descriptiveNote)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(RDA.hasPeriodOfActivityOfAgent)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.agentIsTargetOfCreationRelation)
            .isBlankNodeKind()

        return model
    }

    fun personShape(): ShapeModel {
        val model = ShapeModel("Test")

        val enrichedPerson = model
            .addFocusNode("PersonEnriched")
            .isBlankNodeKind()

        enrichedPerson
            .addProperty(RICO.name)
            .isLiteralNodeKind(RDF.langString)
            .languageIn("de", "fr", "it")
            .uniqueLang()
            .maxCountCardinality(3)
            .minCountCardinality(1)

        enrichedPerson
            .addProperty(RICO.descriptiveNote)
            .isLiteralNodeKind(RDF.langString)
            .languageIn("de", "fr", "it")
            .uniqueLang()
            .maxCountCardinality(3)
            // Note: Not all resources from wikidata have a description! Could be a way to find those that have this
            // missing.
            .minCountCardinality(0)

        enrichedPerson
            .addProperty(SCHEMA.sameAs)
            .isLiteralNodeKind(XSD.string)
            .minCountCardinality(1)

        enrichedPerson
            .addProperty(RICO.isOrWasSubjectOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        enrichedPerson
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern("EnrichedByMemobase|EnrichedByConnectom")

        enrichedPerson
            .addProperty(RICO.resultsOrResultedFrom)
            .isBlankNodeKind()
            .maxCountCardinality(2)
            .minCountCardinality(1)

        val sourcePerson = model
            .addFocusNode("PersonSource")
            .isBlankNodeKind()

        sourcePerson
            .addProperty(RICO.name)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)

        sourcePerson
            .addProperty(FOAF.firstName)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)

        sourcePerson
            .addProperty(FOAF.lastName)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)

        sourcePerson
            .addProperty(SCHEMA.sameAs)
            .isLiteralNodeKind(XSD.string)

        sourcePerson
            .addProperty(RDA.hasVariantNameOfAgent)
            .isLiteralNodeKind(XSD.string)

        sourcePerson
            .addProperty(RICO.history)
            .isLiteralNodeKind(XSD.string)

        sourcePerson
            .addProperty(RDA.hasPeriodOfActivityOfAgent)
            .isBlankNodeKind()

        sourcePerson
            .addProperty(FOAF.gender)
            .isLiteralNodeKind(XSD.string)

        sourcePerson
            .addProperty(RDA.hasProfessionOrOccupation)
            .isLiteralNodeKind(XSD.string)

        sourcePerson
            .addProperty(RDA.isMemberOf)
            .isLiteralNodeKind(XSD.string)

        sourcePerson
            .addProperty(RICO.hasBirthDate)
            // TODO: change this to blank node only once issue https://ub-basel.atlassian.net/browse/MEMO-2157 is fixed
            .isBlankNodeOrLiteralKind()
            .maxCountCardinality(1)

        sourcePerson
            .addProperty(RICO.hasDeathDate)
            // TODO: change this to blank node only once issue https://ub-basel.atlassian.net/browse/MEMO-2157 is fixed
            .isBlankNodeOrLiteralKind()
            .maxCountCardinality(1)

        sourcePerson
            .addProperty(RICO.agentIsTargetOfCreationRelation)
            .isBlankNodeKind()
            .maxCountCardinality(1)

        sourcePerson
            .addProperty(RICO.isOrWasAffectedBy)
            .isBlankNodeKind()
            .maxCountCardinality(1)

        sourcePerson
            // TODO: this is currently never actually added, but probably should where applicable.
            .addProperty(RICO.isOrWasSubjectOf)
            .isBlankNodeKind()
            .maxCountCardinality(1)

        val focusNode = model
            .addFocusNode("Person", false)
            .setTargetClass(RICO.Person)

        focusNode
            .or(
                sourcePerson,
                enrichedPerson,
            )

        return model
    }

    fun corporateBodyShape(): ShapeModel {
        val model = ShapeModel("Test")

        val focusNode = model.addFocusNode("CorporateBody")
            .setTargetClass(RICO.CorporateBody)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.name)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)

        focusNode
            .addProperty(RDA.hasVariantNameOfAgent)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(RICO.history)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(RICO.descriptiveNote)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(RDA.hasPeriodOfActivityOfAgent)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.agentIsTargetOfCreationRelation)
            .isBlankNodeKind()

        focusNode
            .addProperty(SCHEMA.sameAs)
            .isLiteralNodeKind(XSD.string)

        return model
    }

    fun singleDate(): ShapeModel {
        val model = ShapeModel("Test")

        val focusNode = model.addFocusNode("SingleDate")
            .setTargetClass(RICO.SingleDate)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.normalizedDateValue)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
        return model
    }

    fun dateRange(): ShapeModel {
        val model = ShapeModel("Test")

        val normalizedDateShape = model.addFocusNode("DateRangeNormalized")
            .isBlankNodeKind()

        normalizedDateShape
            .addProperty(RICO.normalizedDateValue)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)

        normalizedDateShape
            .addProperty(RICO.isDeathDateOf)
            .isBlankNodeKind()
            .maxCountCardinality(1)

        normalizedDateShape
            .addProperty(RICO.isBirthDateOf)
            .isBlankNodeKind()
            .maxCountCardinality(1)

        normalizedDateShape
            .addProperty(RICO.certainty)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)

        normalizedDateShape
            .addProperty(RICO.dateQualifier)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)

        val expressedDateRange = model.addFocusNode("DateRangeExpressed")
            .isBlankNodeKind()

        expressedDateRange
            .addProperty(RICO.expressedDate)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)

        expressedDateRange
            .addProperty(RICO.certainty)
            .isLiteralNodeKind(XSD.string)

        expressedDateRange
            .addProperty(RICO.dateQualifier)
            .isLiteralNodeKind(XSD.string)

        val focusNode = model.addFocusNode("DateRange", false)
            .setTargetClass(RICO.DateRange)
            .isBlankNodeKind()

        focusNode.or(
            normalizedDateShape,
            expressedDateRange,
        )

        return model
    }

    fun placeShape(): ShapeModel {
        val model = ShapeModel("Test")

        val enrichedPlace = model.addFocusNode("EnrichedPlace")
            .isBlankNodeKind()

        enrichedPlace
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern("EnrichedByMemobase|EnrichedByConnectom")

        enrichedPlace
            .addProperty(RICO.name)
            .isLiteralNodeKind(RDF.langString)
            .maxCountCardinality(3)
            .minCountCardinality(1)
            .uniqueLang()
            .languageIn("de", "fr", "it")

        enrichedPlace
            .addProperty(RICO.descriptiveNote)
            .isLiteralNodeKind(RDF.langString)
            .maxCountCardinality(3)
            .minCountCardinality(3)
            .uniqueLang()
            .languageIn("de", "fr", "it")

        enrichedPlace
            .addProperty(WD.coordinateLocation)
            .isLiteralNodeKind(XSD.string)

        enrichedPlace
            .addProperty(RICO.resultsOrResultedFrom)
            .isBlankNodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        enrichedPlace
            .addProperty(SCHEMA.sameAs)
            .isLiteralNodeKind(XSD.string)
            .minCountCardinality(1)

        enrichedPlace
            .addProperty(RDA.isPlaceOfCaptureOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(0)

        val originPlace = model.addFocusNode("OriginPlace")
            .isBlankNodeKind()

        originPlace
            .addProperty(RICO.name)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)

        originPlace
            .addProperty(RICO.isOrWasAffectedBy)
            .isBlankNodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(0)

        originPlace
            .addProperty(RDA.isPlaceOfCaptureOf)
            .isIRINodeKind()
            .maxCountCardinality(1)

        originPlace
            .addProperty(SCHEMA.sameAs)
            .isLiteralNodeKind(XSD.string)

        val focusNode = model.addFocusNode("Place", false)
            .setTargetClass(RICO.Place)

        focusNode
            .or(
                enrichedPlace,
                originPlace,
            )

        return model
    }

    fun hasPlaceOfCaptureObjectProperty(): ShapeModel {
        val model = ShapeModel("Test")

        val hasPlaceOfCapture = model.addFocusNode("HasPlaceOfCapturePlace")
            .setTargetObjectOf(RDA.hasPlaceOfCapture)
            .isBlankNodeKind()

        hasPlaceOfCapture
            .addProperty(RICO.name)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)

        hasPlaceOfCapture
            .addProperty(RICO.isOrWasAffectedBy)
            .isBlankNodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(0)

        hasPlaceOfCapture
            .addProperty(RDA.isPlaceOfCaptureOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
            // TODO: actually add this to all the resources!
            // .minCountCardinality(1)

        hasPlaceOfCapture
            .addProperty(SCHEMA.sameAs)
            .isLiteralNodeKind(XSD.string)

        return model
    }


    fun languageShape(): ShapeModel {
        val model = ShapeModel("Test")

        val sourceLanguage = model.addFocusNode("LanguageSource")
            .isBlankNodeKind()

        sourceLanguage
            .addProperty(RICO.name)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)

        sourceLanguage
            .addProperty(RICO.isOrWasAffectedBy)
            .isBlankNodeKind()
            // TODO: sometimes language is normalized into two different entities. This should create only one activity.
            // is fixed, wait for it to be deployed and then lower this back to 1!
            .maxCountCardinality(2)
            // TODO: sometimes language is not normalized. Check task.
            .minCountCardinality(0)

        sourceLanguage
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern("content|caption")

        val enrichedLanguage = model.addFocusNode("LanguageEnriched")
            .isBlankNodeKind()

        enrichedLanguage
            .addProperty(RICO.name)
            .isLiteralNodeKind(RDF.langString)
            .maxCountCardinality(3)
            .minCountCardinality(3)
            .uniqueLang()
            .languageIn("de", "fr", "it")

        enrichedLanguage
            .addProperty(SCHEMA.sameAs)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            // not all enriched languages are linked to wikidata
            .minCountCardinality(0)
            .pattern("http://www.wikidata.org/entity/Q\\d{1,10}")

        enrichedLanguage
            .addProperty(RICO.resultsOrResultedFrom)
            .isBlankNodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        enrichedLanguage
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern("content|caption")

        val focusNode = model.addFocusNode("Language", false)
            .setTargetObjectOf(RICO.hasOrHadLanguage)
            .isBlankNodeKind()

        focusNode.or(
            enrichedLanguage,
            sourceLanguage,
        )

        return model
    }

    fun titleShape(): ShapeModel {
        val model = ShapeModel("Test")

        val noLanguageTitle = model.addFocusNode("TitleWithoutLanguage")
            .isBlankNodeKind()

        noLanguageTitle
            .addProperty(RICO.title)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)

        noLanguageTitle
            .addProperty(RICO.isOrWasTitleOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        noLanguageTitle
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern("main|series|broadcast")


        val languageTitle = model.addFocusNode("TitleWithLanguage")
            .isBlankNodeKind()

        languageTitle
            .addProperty(RICO.title)
            .isLiteralNodeKind(RDF.langString)
            .maxCountCardinality(3)
            .minCountCardinality(1)
            .uniqueLang()
            .languageIn("de", "fr", "it")

        languageTitle
            .addProperty(RICO.isOrWasTitleOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        languageTitle
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern("main|series|broadcast")

        val focusNode = model.addFocusNode("Title", false)
            .setTargetClass(RICO.Title)

        focusNode
            .or(
                languageTitle,
                noLanguageTitle,
            )
        return model
    }

    fun identifierShape(): ShapeModel {
        val model = ShapeModel("Test")

        val focusNode = model.addFocusNode("Identifier")
            .setTargetClass(RICO.Identifier)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.identifier)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.isOrWasIdentifierOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern("main|oldMemobase|original|callNumber")

        return model
    }

    fun creationRelationShape(): ShapeModel {
        val model = ShapeModel("Test")

        val focusNode = model.addFocusNode("CreationRelation")
            .setTargetClass(RICO.CreationRelation)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.name)
            .isLiteralNodeKind(XSD.string)
            // TODO: there is one case where creation relation has 2 names:
            .maxCountCardinality(2)
        // TODO: Creation relation should always have a name or no?
        // .minCountCardinality(1)

        focusNode
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern("creator|contributor")

        focusNode
            .addProperty(RICO.creationRelationHasSource)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.creationRelationHasTarget)
            .isBlankNodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        return model
    }

    fun recordResourceHoldingRelationShape(): ShapeModel {
        val model = ShapeModel("Test")

        val focusNode = model.addFocusNode("RecordResourceHoldingRelation")
            .setTargetClass(RICO.RecordResourceHoldingRelation)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern("original|master|access")

        focusNode
            .addProperty(RICO.recordResourceHoldingRelationHasSource)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.recordResourceHoldingRelationHasTarget)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        return model
    }

    fun ruleShape(): ShapeModel {
        val model = ShapeModel("Test")

        val focusNode = model.addFocusNode("Rule")
            .setTargetClass(RICO.Rule)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.name)
            .isLiteralNodeKind(XSD.string)
            // TODO: for srf-029 at least there are two names for each access rule (faro + public) should really be two different rules!
            .maxCountCardinality(2)
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern("access|usage|holder|exifCopyright")

        focusNode
            .addProperty(RICO.regulatesOrRegulated)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        focusNode
            .addProperty(SCHEMA.sameAs)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)

        return model
    }

    fun skosConceptShape(): ShapeModel {
        val model = ShapeModel("Test")

        val enrichedConcept = model.addFocusNode("SkosConceptEnriched")
            .isBlankNodeKind()

        enrichedConcept
            .addProperty(SKOS.prefLabel)
            .isLiteralNodeKind(RDF.langString)
            .maxCountCardinality(3)
            .minCountCardinality(3)
            .languageIn("de", "fr", "it")
            .uniqueLang()

        enrichedConcept
            .addProperty(RICO.resultsOrResultedFrom)
            .isBlankNodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        val sourceConcept = model.addFocusNode("SkosConceptSource")
            .isBlankNodeKind()

        sourceConcept
            .addProperty(SKOS.prefLabel)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)

        sourceConcept
            .addProperty(RICO.isOrWasAffectedBy)
            .isBlankNodeKind()
            // TODO: reduce down to one when normalization only creates one activity for this normalization.
            .maxCountCardinality(3)
            // TODO: should always be normalized. Figure out where it is not.
            .minCountCardinality(0)

        val focusNode = model.addFocusNode("SkosConcept", closed = false)
            .setTargetClass(SKOS.Concept)
            .isBlankNodeKind()

        focusNode
            .or(
                enrichedConcept,
                sourceConcept,
            )

        return model
    }
}