/*
   Copyright 2020 Jonas Waeber

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package ch.memobase.rdf

import ch.memobase.exceptions.InvalidMappingException

/**
 * Utility class for prefixes and namespaces. Each namespace is defined as
 * a constant with the preferred prefix.
 */
object NS {
    const val rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    const val rdfs = "http://www.w3.org/2000/01/rdf-schema#"
    const val owl = "http://www.w3.org/2002/07/owl#"
    const val skos = "http://www.w3.org/2004/02/skos/core#"

    const val rico = "https://www.ica.org/standards/RiC/ontology#"
    const val ebucore = "http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#"
    const val edm = "http://www.europeana.eu/schemas/edm/"

    const val mbcb = "https://memobase.ch/institution/"
    const val mbrs = "https://memobase.ch/recordSet/"
    const val mbr = "https://memobase.ch/record/"
    const val mbpo = "https://memobase.ch/physical/"
    const val mbdo = "https://memobase.ch/digital/"

    const val internal = "https://memobase.ch/internal/"

    const val dc = "http://purl.org/dc/elements/1.1/"
    const val dcterms = "http://purl.org/dc/terms/"

    const val schema = "http://schema.org/"
    const val foaf = "http://xmlns.com/foaf/0.1/"
    const val wd = "http://www.wikidata.org/entity/"
    const val wdt = "http://www.wikidata.org/prop/direct/"
    const val wdtn = "http://www.wikidata.org/prop/direct-normalized/"

    const val xsd = "http://www.w3.org/2001/XMLSchema#"
    const val geo = "http://www.opengis.net/ont/geosparql#"
    const val rdau = "http://rdaregistry.info/Elements/u/"

    const val ldp = "http://www.w3.org/ns/ldp#"
    const val fedora = "http://fedora.info/definitions/v4/repository#"
    const val shacl = "http://www.w3.org/ns/shacl#"

    /**
     * A set of defined prefix mappings as used in the memobase project.
     */
    val prefixMapping = mapOf(
        Pair("rdf", rdf),
        Pair("rdfs", rdfs),
        Pair("owl", owl),
        Pair("skos", skos),
        Pair("rico", rico),
        Pair("ebucore", ebucore),
        Pair("edm", edm),
        Pair("dc", dc),
        Pair("dcterms", dcterms),
        Pair("schema", schema),
        Pair("foaf", foaf),
        Pair("wdt", wdt),
        Pair("wdtn", wdtn),
        Pair("rdau", rdau),
        Pair("fedora", fedora),
        Pair("ldp", ldp),
        Pair("wd", wd),
        Pair("internal", internal),
        Pair("mbcb", mbcb),
        Pair("mbdo", mbdo),
        Pair("mbpo", mbpo),
        Pair("mbr", mbr),
        Pair("mbrs", mbrs),
        Pair("xsd", xsd),
        Pair("sh", shacl),
        Pair("geo", geo)
    )

    /**
     * A utility function to get the expanded namespace for the given prefix.
     *
     * @param prefix A known prefix used in the memobase data model.
     * @return The namespace belonging to this prefix.
     * @throws InvalidMappingException If the prefix is not in prefixMapping
     */
    fun prefixToNamespace(prefix: String): String {
        return prefixMapping[prefix].let {
            it ?: throw InvalidMappingException("Unknown prefix $prefix. No mapping to namespace defined currently!")
        }
    }

    /**
     * A utility function to get the prefix for the given namespace uri.
     *
     * @param namespace A known namespace used in the memobase data model.
     * @return The prefix belonging to this namespace.
     * @throws InvalidMappingException If the prefix is not in prefixMapping
     */
    fun namespaceToPrefix(namespace: String): String {
        val entry = prefixMapping.entries.find { namespace == it.value }
        if (entry != null) return entry.key
        else throw InvalidMappingException("Unknown namespace $namespace. No mapping to prefix defined currently!")
    }
}
