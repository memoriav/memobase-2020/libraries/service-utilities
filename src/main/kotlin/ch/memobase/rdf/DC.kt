/*
   Copyright 2020 Jonas Waeber

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package ch.memobase.rdf

import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.ResourceFactory

/**
 * Dublin Core (DC) properties
 */
object DC {
    /**
     * dcterms:abstract
     */
    val abstract: Property = prop("abstract")

    /**
     * dcterms:created
     */
    val created: Property = prop("created")

    /**
     * dcterms:issued
     */
    val issued: Property = prop("issued")

    /**
     * dcterms:temporal
     */
    val temporal: Property = prop("temporal")

    /**
     * dcterms:spatial
     */
    val spatial: Property = prop("spatial")

    /**
     * dcterms:relation
     */
    val relation: Property = prop("relation")

    /**
     * dcterms:conformsTo
     */
    val conformsTo: Property = prop("conformsTo")

    private fun prop(name: String): Property {
        return ResourceFactory.createProperty(NS.dcterms, name)
    }

    private fun res(name: String): Resource {
        return ResourceFactory.createResource(NS.dcterms + name)
    }
}
