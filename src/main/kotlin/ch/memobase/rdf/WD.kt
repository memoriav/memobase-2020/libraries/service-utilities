/*
   Copyright 2020 Jonas Waeber

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package ch.memobase.rdf

import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.ResourceFactory

object WD {

    /**
     * physical location entity id (Q17334923)
     */
    const val PHYSICAL_LOCATION_Q17334923 = "Q17334923"

    /**
     * physical location (Q17334923)
     */
    @Deprecated("Renamed to physicalLocation.", ReplaceWith("physicalLocation"))
    val location = res(PHYSICAL_LOCATION_Q17334923)

    /**
     * physical location (Q17334923)
     */
    val physicalLocation = res(PHYSICAL_LOCATION_Q17334923)

    /**
     * Switzerland entity id (Q39)
     */
    val SWITZERLAND_Q39 = "Q39"

    /**
     * Switzerland (Q39)
     */
    val switzerland = res(SWITZERLAND_Q39)

    /**
     * ISIL property id (P791)
     */
    const val ISIL_P791 = "P791"

    /**
     * ISIL (P791)
     */
    val isil = prop(ISIL_P791)

    /**
     * located on streed property id (P669)
     */
    const val LOCATED_ON_STREET_P669 = "P669"

    /**
     * located on street (P669)
     */
    @Deprecated("Renamed to locatedOnStreet.", ReplaceWith("locatedOnStreet"))
    val street = prop(LOCATED_ON_STREET_P669)

    /**
     * located on street (P669)
     */
    val locatedOnStreet = prop(LOCATED_ON_STREET_P669)

    /**
     * house number property id (P670)
     */
    const val HOUSE_NUMBER_P670 = "P670"

    /**
     * house number (P670)
     */
    @Deprecated("Renamed to houseNumber.", ReplaceWith("houseNumber"))
    val streetNumber = prop(HOUSE_NUMBER_P670)

    /**
     * house number (P670)
     */
    val houseNumber = prop(HOUSE_NUMBER_P670)

    /**
     * street address property id (P6375)
     */
    const val STREET_ADDRESS_P6375 = "P6375"

    /**
     * street address (P6375)
     */
    val streetAddress = prop(STREET_ADDRESS_P6375)

    /**
     * located in the administrative territorial entity property id (P131)
     */
    const val LOCATED_IN_THE_ADMINISTRATIVE_TERRITORIAL_ENTITY_P131 = "P131"

    /**
     * located in the administrative territorial entity (P131)
     */
    @Deprecated(
        "Renamed to locatedInTheAdministrativeTerritorialEntity",
        ReplaceWith("locatedInTheAdministrativeTerritorialEntity")
    )
    val adminUnit = prop(LOCATED_IN_THE_ADMINISTRATIVE_TERRITORIAL_ENTITY_P131)

    /**
     * located in the administrative territorial entity (P131)
     */
    val locatedInTheAdministrativeTerritorialEntity = prop(LOCATED_IN_THE_ADMINISTRATIVE_TERRITORIAL_ENTITY_P131)

    /**
     * postal code property id (P281)
     */
    const val POSTAL_CODE_P281 = "P281"

    /**
     * postal code (P281)
     */
    val postalCode = prop(POSTAL_CODE_P281)

    /**
     * country property id (P17)
     */
    const val COUNTRY_P17 = "P17"

    /**
     * country (P17)
     */
    val country = prop(COUNTRY_P17)

    /**
     * official website property id (P856)
     */
    const val OFFICIAL_WEBSITE_P856 = "P856"

    /**
     * official website (P856)
     */
    val website = prop(OFFICIAL_WEBSITE_P856)


    /**
     * email address property id (P968)
     */
    const val EMAIL_ADDRESS_P968 = "P968"

    /**
     * email address (P968): Prefixed with 'mailto:'
     */
    val emailAddress = prop(EMAIL_ADDRESS_P968)

    /**
     * URL property id (P2699)
     */
    const val URL_P2699 = "P2699"

    /**
     * URL (P2699)
     */
    @Deprecated("Renamed to URL.", ReplaceWith("URL"))
    val onlineArchive = prop(URL_P2699)

    /**
     * URL (P2699)
     */
    val URL = prop(URL_P2699)

    /**
     * coordinate location property id (P625)
     */
    const val COORDINATE_LOCATION_P625 = "P625"

    /**
     * coordinate location (P625)
     */
    @Deprecated(
        "Use coordinateLocation property instead. Just renamed to better reflect the name of the property.",
        ReplaceWith("coordinateLocation")
    )
    val coordinates = prop(COORDINATE_LOCATION_P625)

    /**
     * coordinate location (P625)
     */
    val coordinateLocation = prop(COORDINATE_LOCATION_P625)

    /**
     * coordinates of geographic center property id (P5140)
     */
    const val COORDINATES_OF_GEOGRAPHIC_CENTER_P5140 = "P5140"

    /**
     * coordinates of geographic center (P5140)
     */
    val coordinatesOfGeographicCenter = prop(COORDINATES_OF_GEOGRAPHIC_CENTER_P5140)

    /**
     * image property id (P18)
     */
    const val IMAGE_P18 = "P18"

    /**
     * image (P18)
     * image of relevant illustration of the subject
     */
    val image = prop(IMAGE_P18)

    /**
     * logo image property id (P154)
     */
    const val LOGO_IMAGE_P154 = "P154"

    /**
     * logo image (P154)
     */
    @Deprecated("Renamed to logoImage.", ReplaceWith("logoImage"))
    val logo = prop(LOGO_IMAGE_P154)

    /**
     * logo image (P154)
     */
    val logoImage = prop(LOGO_IMAGE_P154)

    /**
     * instance of property id (P31)
     */
    const val INSTANCE_OF_P31 = "P31"

    /**
     * instance of (P31)
     */
    val instanceOf = prop(INSTANCE_OF_P31)

    /**
     * instance of (P31)
     */
    @Deprecated("Renamed to instanceOf.", ReplaceWith("instanceOf"))
    val typeOfInstitution = prop(INSTANCE_OF_P31)


    /**
     * HDS ID property id (P902)
     */
    const val HDS_ID_P902 = "P902"

    /**
     * HDS ID (P902)
     */
    val hdsId = prop(HDS_ID_P902)

    /**
     * GND ID property id (P227)
     */
    const val GND_ID_P227 = "P227"

    /**
     * GND ID (P227)
     */
    val gndId = prop(GND_ID_P227)

    private fun prop(name: String): Property {
        return ResourceFactory.createProperty(NS.wdt, name)
    }

    fun res(name: String): Resource {
        return ResourceFactory.createResource(NS.wd + name)
    }
}
