/*
   Copyright 2020 Jonas Waeber

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package ch.memobase.rdf

import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.ResourceFactory

/**
 * Documentation Memobase:
 * https://ub-basel.atlassian.net/wiki/spaces/MD/pages/336855177/Memobase+RDF
 * Documentation RICO:
 * https://www.ica.org/standards/RiC/RiC-O_v0-2.html
 */
object RICO {
    val RecordSet = res("RecordSet")
    val Record = res("Record")
    val Instantiation = res("Instantiation")
    val Title = res("Title")
    val Language = res("Language")
    val Identifier = res("Identifier")
    val Place = res("Place")
    val Agent = res("Agent")
    val Person = res("Person")
    val CorporateBody = res("CorporateBody")
    val RecordResourceHoldingRelation = res("RecordResourceHoldingRelation")
    val Rule = res("Rule")
    val CarrierType = res("CarrierType")
    val ContentType = res("ContentType")
    val Extent = res("Extent")

    val CreationRelation = res("CreationRelation")

    val SingleDate = res("SingleDate")
    val DateRange = res("DateRange")
    val DateSet = res("DateSet")

    // datatype properties

    val title: Property = prop("title")
    val identifier: Property = prop("identifier")
    val source: Property = prop("source")
    val descriptiveNote: Property = prop("descriptiveNote")
    val name: Property = prop("name")
    val type: Property = prop("type")
    val expressedDate: Property = prop("expressedDate")
    val normalizedDateValue: Property = prop("normalizedDateValue")
    val normalizedValue: Property = prop("normalizedValue")
    val publicationDate: Property = prop("publicationDate")
    val modificationDate: Property = prop("modificationDate")
    val scopeAndContent: Property = prop("scopeAndContent")
    val physicalCharacteristics: Property = prop("physicalCharacteristics")
    val technicalCharacteristics: Property = prop("technicalCharacteristics")
    val conditionsOfUse: Property = prop("conditionsOfUse")
    val conditionsOfAccess: Property = prop("conditionsOfAccess")

    val birthDate: Property = prop("birthDate")
    val deathDate: Property = prop("deathDate")
    val dateQualifier: Property = prop("dateQualifier")
    val certainty: Property = prop("certainty")
    val history: Property = prop("history")
    val integrity: Property = prop("integrity")
    val recordResourceExtent: Property = prop("recordResourceExtent")

    val quantity: Property = prop("quantity")
    val unitOfMeasurement: Property = prop("unitOfMeasurement")

    // Object Properties

    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val hasTitle: Property = prop("hasOrHadTitle")
    val hasOrHadTitle: Property = prop("hasOrHadTitle")
    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val isTitleOf: Property = prop("isOrWasTitleOf")
    val isOrWasTitleOf: Property = prop("isOrWasTitleOf")

    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val hasLanguage: Property = prop("hasOrHadLanguage")
    val hasOrHadLanguage: Property = prop("hasOrHadLanguage")
    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val isLanguageOf: Property = prop("isOrWasLanguageOf")
    val isOrWasLanguageOf: Property = prop("isOrWasLanguageOf")

    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val identifiedBy: Property = prop("hasOrHadIdentifier")
    val hasOrHadIdentifier: Property = prop("hasOrHadIdentifier")
    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val identifies: Property = prop("isOrWasIdentifierOf")
    val isOrWasIdentifierOf: Property = prop("isOrWasIdentifierOf")

    val hasExtent: Property = prop("hasExtent")
    val isExtentOf: Property = prop("isExtentOf")

    val isBirthDateOf: Property = prop("isBirthDateOf")
    val hasBirthDate: Property = prop("hasBirthDate")
    val isDeathDateOf: Property = prop("isDeathDateOf")
    val hasDeathDate: Property = prop("hasDeathDate")

    val hasSource: Property = prop("hasSource")
    val isSourceOf: Property = prop("isSourceOf")

    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val hasSubject: Property = prop("hasOrHadSubject")
    val hasOrHadSubject: Property = prop("hasOrHadSubject")
    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val isSubjectOf: Property = prop("isOrWasSubjectOf")
    val isOrWasSubjectOf: Property = prop("isOrWasSubjectOf")

    val hasCarrierType: Property = prop("hasCarrierType")
    val isCarrierTypeOf: Property = prop("isCarrierTypeOf")

    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val hasLocation: Property = prop("hasOrHadLocation")
    val hasOrHadLocation: Property = prop("hasOrHadLocation")
    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val isLocationOf: Property = prop("isOrWasLocationOf")
    val isOrWasLocationOf: Property = prop("isOrWasLocationOf")

    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val publishedBy: Property = prop("hasPublisher")
    val hasPublisher: Property = prop("hasPublisher")
    val isPublisherOf: Property = prop("isPublisherOf")

    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val heldBy: Property = prop("hasOrHadHolder")
    val hasOrHadHolder: Property = prop("hasOrHadHolder")
    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val isHolderOf: Property = prop("isOrWasHolderOf")
    val isOrWasHolderOf: Property = prop("isOrWasHolderOf")

    val hasOrHadPart: Property = prop("hasOrHadPart")
    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val isPartOf: Property = prop("isOrWasPartOf")
    val isOrWasPartOf: Property = prop("isOrWasPartOf")

    val hasProvenance: Property = prop("hasProvenance")
    val isProvenanceOf: Property = prop("isProvenanceOf")

    val hasInstantiation: Property = prop("hasInstantiation")
    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val instantiates: Property = prop("isInstantiationOf")
    val isInstantiationOf: Property = prop("isInstantiationOf")

    val hasDerivedInstantiation: Property = prop("hasDerivedInstantiation")
    val isDerivedFromInstantiation: Property = prop("isDerivedFromInstantiation")

    val creationRelationHasSource: Property = prop("creationRelationHasSource")
    val recordResourceOrInstantiationIsSourceOfCreationRelation: Property =
        prop("recordResourceOrInstantiationIsSourceOfCreationRelation")

    val creationRelationHasTarget: Property = prop("creationRelationHasTarget")
    val agentIsTargetOfCreationRelation: Property = prop("agentIsTargetOfCreationRelation")

    /**
     * This is a symmetric property.
     */
    val isRecordResourceAssociatedWithRecordResource: Property = prop("isRecordResourceAssociatedWithRecordResource")

    val recordResourceHoldingRelationHasSource: Property = prop("recordResourceHoldingRelationHasSource")
    val agentIsSourceOfRecordResourceHoldingRelation: Property = prop("agentIsSourceOfRecordResourceHoldingRelation")

    val recordResourceHoldingRelationHasTarget: Property = prop("recordResourceHoldingRelationHasTarget")
    val recordResourceOrInstantiationIsTargetOfRecordResourceHoldingRelation: Property =
        prop("recordResourceOrInstantiationIsTargetOfRecordResourceHoldingRelation")

    val hasOrHadAllMembersWithContentType: Property = prop("hasOrHadAllMembersWithContentType")
    val isOrWasContentTypeOfAllMembersOf: Property = prop("isOrWasContentTypeOfAllMembersOf")


    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val intellectualPropertyRightsHeldBy: Property = prop("hasOrHadIntellectualPropertyRightsHolder")
    val hasOrHadIntellectualPropertyRightsHolder: Property = prop("hasOrHadIntellectualPropertyRightsHolder")
    val isOrWasHolderOfIntellectualPropertyRightsOf: Property = prop("isOrWasHolderOfIntellectualPropertyRightsOf")

    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val regulates: Property = prop("regulatesOrRegulated")
    val regulatesOrRegulated: Property = prop("regulatesOrRegulated")
    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val regulatedBy: Property = prop("isOrWasRegulatedBy")
    val isOrWasRegulatedBy: Property = prop("isOrWasRegulatedBy")

    val isDateAssociatedWith: Property = prop("isDateAssociatedWith")
    val isAssociatedWithDate: Property = prop("isAssociatedWithDate")

    /**
     * [Definition](https://www.ica.org/standards/RiC/ontology#Mechanism)
     */
    val Mechanism: Resource = res("Mechanism")

    /**
     * [Definition](https://www.ica.org/standards/RiC/RiC-O_v0-2.html#Activity)
     */
    val Activity: Resource = res("Activity")

    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val resultsFrom: Property = prop("resultsOrResultedFrom")
    val resultsOrResultedFrom: Property = prop("resultsOrResultedFrom")
    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val resultsIn: Property = prop("resultsOrResultedIn")
    val resultsOrResultedIn: Property = prop("resultsOrResultedIn")

    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val affects: Property = prop("affectsOrAffected")
    val affectsOrAffected: Property = prop("affectsOrAffected")
    val isOrWasAffectedBy: Property = prop("isOrWasAffectedBy")

    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val performedBy: Property = prop("isOrWasPerformedBy")
    val isOrWasPerformedBy: Property = prop("isOrWasPerformedBy")
    @Deprecated("RICO v0.1 property name. Output is v0.2.")
    val performs: Property = prop("performsOrPerformed")
    val performsOrPerformed: Property = prop("performsOrPerformed")

    val beginningDate: Property = prop("beginningDate")
    val endDate: Property = prop("endDate")

    object Types {
        object Record {
            const val publication = "publication"
            const val related = "related"
            const val Film = "Film"
            const val Foto = "Foto"
            const val Radio = "Radio"
            const val Ton = "Ton"
            const val Tonbildschau = "Tonbildschau"
            const val TV = "TV"
            const val Video = "Video"
            val validRecordTypeValues = listOf(Film, Foto, Radio, Ton, Tonbildschau, TV, Video)

        }
        object Instantiation {
            const val physicalObject = "physicalObject"
            const val digitalObject = "digitalObject"
            const val thumbnail = "thumbnail"

        }
        object RecordSet {
            const val original = "original"
            const val related = "related"
        }
        object CorporateBody {
            const val memobaseInstitution = "memobaseInstitution"
            const val memoriavProject = "memoriavProject"
            const val original = "original"
            const val access = "access"
            const val master = "master"
            const val memobaseInstitutionInventoryProject = "memobaseInstitutionInventoryProject"
        }
        object Place {
            const val mainAddress = "mainAddress"
            const val additionalAddress = "additionalAddress"
            const val canton = "canton"
            const val municipality = "municipality"
        }
        object Language {
            const val content = "content"
            const val caption = "caption"
            const val metadata = "metadata"
        }
        object Title {
            const val main = "main"
            const val series = "series"
            const val broadcast = "broadcast"
            const val original = "original"
        }
        object Identifier {
            const val main = "main"
            const val oldMemobase = "oldMemobase"
            const val callNumber = "callNumber"
            const val original = "original"
        }
        object CreationRelation {
            const val creator = "creator"
            const val contributor = "contributor"
            const val exifArtist = "exifArtist"
        }
        object RecordResourceHoldingRelation {
            const val original = "original"
            const val master = "master"
            const val access = "access"
        }
        object Rule {
            const val usage = "usage"
            const val holder = "holder"
            const val access = "access"
        }
        object Extent {
            const val physicalFormats = "physicalObjects"
            const val digitalFormats = "digitalObjects"
            const val digitizedShare = "physicalObjectsDigitized"
            const val catalogedShare = "objectsCatalogued"
        }
    }

    object UnitOfMeasurement {
        const val amountEstimated = "Amount (estimated)"
        const val percentageEstimated = "Percentage (estimated)"
    }

    private fun prop(name: String): Property {
        return ResourceFactory.createProperty(NS.rico, name)
    }

    private fun res(name: String): Resource {
        return ResourceFactory.createResource(NS.rico + name)
    }
}
