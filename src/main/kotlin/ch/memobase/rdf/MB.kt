/*
   Copyright 2020 Jonas Waeber

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package ch.memobase.rdf

import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.ResourceFactory

object MB {

    /**
     *
     */
    val eventType = prop("eventType")
    val isPublished = prop("isPublished")
    val proxyType = prop("proxyType")
    val institutionTeaserColor = prop("institutionTeaserColor")
    val institutionComputedTeaserColor = prop("institutionComputedTeaserColor")
    val recordSetProcessedTeaserText = prop("recordSetProcessedTeaserText")
    val fullInstitutionPage = prop("fullInstitutionPage")
    val surveyInstitution = prop("surveyInstitution")
    val publicationPermitted = prop("publicationPermitted")

    /**
     * The internal value stores if the media resource is 'local' or 'remote'. This
     * is set by the JSON-LD serializer and used by the search doc transformer.
     */
    val mediaLocation = prop("mediaLocation")

    /**
     * The unique URI of the Memoriav institution.
     */
    const val memoriavInstitutionURI = NS.mbcb + "mav"

    val memoriavInstitution: Resource = ResourceFactory.createResource(memoriavInstitutionURI)

    private fun prop(name: String): Property {
        return ResourceFactory.createProperty(NS.internal, name)
    }
}
