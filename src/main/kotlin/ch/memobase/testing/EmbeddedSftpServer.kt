/*
   Copyright 2020 Jonas Waeber

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package ch.memobase.testing

import com.github.marschall.memoryfilesystem.MemoryFileSystemBuilder.newLinux
import org.apache.sshd.common.file.FileSystemFactory
import org.apache.sshd.common.session.SessionContext
import java.io.Closeable
import java.io.IOException
import java.io.InputStream
import java.nio.charset.Charset
import java.nio.file.FileStore
import java.nio.file.FileSystem
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.PathMatcher
import java.nio.file.WatchService
import java.nio.file.attribute.UserPrincipalLookupService
import java.nio.file.spi.FileSystemProvider
import org.apache.sshd.server.SshServer
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider
import org.apache.sshd.sftp.server.SftpSubsystemFactory

class EmbeddedSftpServer(port: Int, user: String, password: String) : Closeable {
    private val credentials = mapOf(Pair(user, password))
    private val fileSystem = newLinux().build("EmbeddedSftpServerFileSystem@" + hashCode())
    private val server: SshServer = SshServer.setUpDefaultServer()

    init {
        server.port = port
        server.keyPairProvider = SimpleGeneratorHostKeyProvider()
        server.setPasswordAuthenticator { authUser, authPassword, _ -> authenticate(authUser, authPassword) }
        server.subsystemFactories = listOf(SftpSubsystemFactory())
        /* When a channel is closed SshServer calls close() on the file system.
         * In order to use the file system for multiple channels/sessions we
         * have to use a file system wrapper whose close() does nothing.
         */
        server.fileSystemFactory = SimulatedFileSystemFactory(fileSystem)
        server.start()
    }

    /**
     * Put a text file on the SFTP folder. The file is available by the
     * specified path.
     * @param path the path to the file.
     * @param content the files content.
     * @param encoding the encoding of the file.
     * @throws IOException if the file cannot be written.
     */
    @Throws(IOException::class)
    fun putFile(
        path: String,
        content: String,
        encoding: Charset = Charset.defaultCharset()
    ) {
        val contentAsBytes = content.toByteArray(encoding)
        putFile(path, contentAsBytes)
    }

    /**
     * Put a file on the SFTP folder. The file is available by the specified
     * path.
     * @param path the path to the file.
     * @param content the files content.
     * @throws IOException if the file cannot be written.
     */
    @Throws(IOException::class)
    fun putFile(
        path: String,
        content: ByteArray
    ) {
        val pathAsObject = fileSystem.getPath(path)
        ensureDirectoryOfPathExists(pathAsObject)
        Files.write(pathAsObject, content)
    }

    /**
     * Put a file on the SFTP folder. The file is available by the specified
     * path. The file content is read from an `InputStream`.
     * @param path the path to the file.
     * @param inputStream an `InputStream` that provides the file's content.
     * @throws IOException if the file cannot be written or the input stream
     * cannot be read.
     */
    @Throws(IOException::class)
    fun putFile(
        path: String,
        inputStream: InputStream
    ) {
        val pathAsObject = fileSystem.getPath(path)
        ensureDirectoryOfPathExists(pathAsObject)
        Files.copy(inputStream, pathAsObject)
    }

    /**
     * Create a directory on the SFTP server.
     * @param path the directory's path.
     * @throws IOException if the directory cannot be created.
     */
    @Throws(IOException::class)
    fun createDirectory(
        path: String
    ) {
        val pathAsObject = fileSystem.getPath(path)
        Files.createDirectories(pathAsObject)
    }

    /**
     * Create multiple directories on the SFTP server.
     * @param paths the directories' paths.
     * @throws IOException if at least one directory cannot be created.
     */
    @Throws(IOException::class)
    fun createDirectories(
        vararg paths: String
    ) {
        for (path in paths) createDirectory(path)
    }

    /**
     * Get a text file from the SFTP server.
     * @param path the path to the file.
     * @return the content of the text file.
     * @throws IOException if the file cannot be read.
     * @throws IllegalStateException if not called from within a test.
     */
    @Throws(IOException::class)
    fun getFileContent(
        path: String,
        encoding: Charset = Charset.defaultCharset()
    ): String {
        return getFileContent(path).toString(encoding)
    }

    /**
     * Get a file from the SFTP server.
     * @param path the path to the file.
     * @return the content of the file.
     * @throws IOException if the file cannot be read.
     * @throws IllegalStateException if not called from within a test.
     */
    @Throws(IOException::class)
    fun getFileContent(
        path: String
    ): ByteArray {
        val pathAsObject = fileSystem.getPath(path)
        return Files.readAllBytes(pathAsObject)
    }

    /**
     * Checks the existence of a file. returns `true` iff the file exists
     * and it is not a directory.
     * @param path the path to the file.
     * @return `true` iff the file exists and it is not a directory.
     * @throws IllegalStateException if not called from within a test.
     */
    fun existsFile(
        path: String
    ): Boolean {
        val pathAsObject = fileSystem.getPath(path)
        return Files.exists(pathAsObject) && !Files.isDirectory(pathAsObject)
    }

    private fun authenticate(
        username: String,
        password: String
    ): Boolean {
        return (credentials.isEmpty() ||
                credentials[username] == password)
    }

    @Throws(IOException::class)
    private fun ensureDirectoryOfPathExists(
        path: Path
    ) {
        val directory = path.parent
        if (directory != null && directory != path.root) Files.createDirectories(directory)
    }


    private open class SimulatedFileSystemFactory(private val fileSystem: FileSystem) : FileSystemFactory {
        override fun getUserHomeDir(session: SessionContext?): Path {
            return fileSystem.getPath("/")
        }

        override fun createFileSystem(session: SessionContext?): FileSystem {
            return DoNotClose(fileSystem)
        }

    }

    private class DoNotClose(
        val fileSystem: FileSystem
    ) : FileSystem() {
        override fun provider(): FileSystemProvider {
            return fileSystem.provider()
        }

        @Throws(IOException::class)
        override fun close() {
            // will not be closed
        }

        override fun isOpen(): Boolean {
            return fileSystem.isOpen
        }

        override fun isReadOnly(): Boolean {
            return fileSystem.isReadOnly
        }

        override fun getSeparator(): String {
            return fileSystem.separator
        }

        override fun getRootDirectories(): Iterable<Path> {
            return fileSystem.rootDirectories
        }

        override fun getFileStores(): Iterable<FileStore> {
            return fileSystem.fileStores
        }

        override fun supportedFileAttributeViews(): Set<String> {
            return fileSystem.supportedFileAttributeViews()
        }

        override fun getPath(
            first: String,
            vararg more: String
        ): Path {
            return fileSystem.getPath(first, *more)
        }

        override fun getPathMatcher(
            syntaxAndPattern: String
        ): PathMatcher {
            return fileSystem.getPathMatcher(syntaxAndPattern)
        }

        override fun getUserPrincipalLookupService(): UserPrincipalLookupService {
            return fileSystem.userPrincipalLookupService
        }

        @Throws(IOException::class)
        override fun newWatchService(): WatchService {
            return fileSystem.newWatchService()
        }
    }

    override fun close() {
        this.fileSystem.close()
        this.server.close()
    }
}
