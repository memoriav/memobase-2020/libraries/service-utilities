package ch.memobase.sftp

import java.io.IOException
import java.io.InputStream
import net.schmizz.sshj.sftp.RemoteFile
import net.schmizz.sshj.sftp.RemoteFile.RemoteFileInputStream

class RemoteInputStream(private val remoteFile: RemoteFile, private val inputStream: RemoteFileInputStream) :
        InputStream() {
    override fun markSupported(): Boolean {
        return true
    }

    private var isClosed = false

    override fun mark(readLimit: Int) {
        inputStream.mark(readLimit)
    }

    @Throws(IOException::class)
    override fun reset() {
        inputStream.reset()
    }

    @Throws(IOException::class)
    override fun skip(n: Long): Long {
        return inputStream.skip(n)
    }

    @Throws(IOException::class)
    override fun read(): Int {
        return inputStream.read()
    }

    @Throws(IOException::class)
    override fun read(into: ByteArray, off: Int, len: Int): Int {
        return inputStream.read(into, off, len)
    }

    override fun close() {
        inputStream.close()
        if (!isClosed) {
            remoteFile.close()
            isClosed = true
        }
        super.close()
    }
}
