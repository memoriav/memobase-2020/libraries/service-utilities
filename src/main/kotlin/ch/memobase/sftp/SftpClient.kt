/*
   Copyright 2020 Jonas Waeber

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package ch.memobase.sftp

import ch.memobase.exceptions.SftpClientException
import ch.memobase.settings.SftpSettings
import java.io.Closeable
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.net.ConnectException
import java.net.UnknownHostException
import net.schmizz.sshj.SSHClient
import net.schmizz.sshj.sftp.FileAttributes
import net.schmizz.sshj.sftp.FileMode
import net.schmizz.sshj.sftp.OpenMode.READ
import net.schmizz.sshj.sftp.SFTPClient
import net.schmizz.sshj.sftp.SFTPException
import net.schmizz.sshj.transport.verification.PromiscuousVerifier
import net.schmizz.sshj.userauth.UserAuthException
import org.apache.logging.log4j.LogManager

/**
 *  A small wrapper around the net.schmizz.sshj.SFTPClient.
 *
 *  @param sftpSettings The settings required to establish a connection.
 *  @throws SftpClientException Rethrows all exceptions caught during initial connection establishment.
 */
class SftpClient(sftpSettings: SftpSettings) : Closeable {
    private val log = LogManager.getLogger("SftpClient")
    private val ssh = SSHClient()
    private val instance: SFTPClient

    init {
        instance = connectToSFTPServer(sftpSettings)
    }

    /**
     * List the files in the given path. The path must be a existing folder. Ignores subfolders.
     *
     * @param path Relative or absolute path on the sftp server. The relative path will be applied to
     *             where the user used is redirected to.
     * @return A list of all regular files.
     * @throws SftpClientException
     */
    @Throws(SftpClientException::class)
    fun listFiles(path: String): List<String> {
        try {
            return ls(path).filter { fileAttributes(it).mode.type == FileMode.Type.REGULAR }
        } catch (ex: SFTPException) {
            throw SftpClientException(ex.localizedMessage)
        }
    }

    /**
     *  Opens an input stream of a file on the server.
     *
     *  @param file A file on the sftp server.
     *
     *  @return The remote file wrapper
     *  @throws SftpClientException
     */
    fun open(file: File): InputStream {
        try {
            val remote = instance.open(file.path, setOf(READ))
            return RemoteInputStream(remote, remote.RemoteFileInputStream())
        } catch (ex: SFTPException) {
            throw SftpClientException(ex.localizedMessage)
        }
    }

    /**
     * Saves a file on the server.
     *
     * @param sourcePath: Path to file on local system
     * @param destPath: Path to file on sFTP server
     *
     * @throws SftpClientException
     */
    fun put(sourcePath: String, destPath: String) {
        try {
            instance.put(sourcePath, destPath)
        } catch (ex: IOException) {
            throw SftpClientException(ex.localizedMessage)
        }
    }

    /**
     *  Checks if a file on the server exists and is a regular file (e.g. not a directory).
     *
     *  @param path Path to the file on the server.
     *  @return true if the file exists and is a regular file otherwise false.
     */
    fun exists(path: String): Boolean {
        return try {
            instance.lstat(path).type == FileMode.Type.REGULAR
        } catch (ex: SFTPException) {
            false
        }
    }

    private fun connectToSFTPServer(sftpSettings: SftpSettings, retries: Int = 0): SFTPClient {
        try {
            log.info("Connecting to sFTP server")
            ssh.addHostKeyVerifier(PromiscuousVerifier())
            ssh.connect(sftpSettings.host, sftpSettings.port)
            ssh.authPassword(sftpSettings.user, sftpSettings.password)
            return ssh.newSFTPClient()
        } catch (ex: UserAuthException) {
            log.error("SFTP User Authentication Error: Invalid user authentication supplied.")
            throw SftpClientException("SFTP User Authentication Error: Invalid user authentication supplied.")
        } catch (ex: ConnectException) {
            log.error("SFTP Connection Exception: ${ex.message}.")
            retryConnect(retries, 3, sftpSettings)
            throw SftpClientException("SFTP Connection Exception: ${ex.message}.")
        } catch (ex: UnknownHostException) {
            log.error("SFTP Host Not Found: ${ex.message}.")
            throw SftpClientException("SFTP Host Not Found: ${ex.message}.")
        } catch (ex: Exception) {
            ex.printStackTrace()
            log.error("SSH Exception: ${ex.localizedMessage}")
            retryConnect(retries, 3, sftpSettings)
            throw SftpClientException("SSH Exception: ${ex.message}.")
        }
    }

    private fun retryConnect(retries: Int, retryLimit: Int, sftpSettings: SftpSettings) {
        if (retries < retryLimit) {
            val waitingTime = 20000L * (retries + 1)
            log.info("Waiting $waitingTime ms before retry")
            Thread.sleep(waitingTime)
            connectToSFTPServer(sftpSettings, if (retries >= 0) retries + 1 else retries)
        }
    }

    private fun fileAttributes(path: String): FileAttributes {
        return instance.lstat(path)
    }

    private fun ls(path: String): List<String> {
        return instance.ls(path).map { it.path }
    }

    override fun close() {
        log.info("Closing connection to sFTP server")
        instance.close()
        ssh.disconnect()
    }
}
