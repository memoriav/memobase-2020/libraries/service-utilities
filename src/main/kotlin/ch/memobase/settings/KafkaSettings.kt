/*
   Copyright 2020 Jonas Waeber

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package ch.memobase.settings

import org.apache.kafka.clients.admin.AdminClientConfig
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.config.SslConfigs
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.streams.StreamsConfig
import org.apache.logging.log4j.Logger
import java.util.*

class KafkaSettings(private val settings: Properties, private val map: Map<String, String>, private val log: Logger) {

    fun consumer() {
        setKafkaProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
        setKafkaProperty(ConsumerConfig.ALLOW_AUTO_CREATE_TOPICS_CONFIG, false)
        setKafkaProperty(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG)
        setKafkaProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, abortIfMissing = true)
        setKafkaProperty(ConsumerConfig.CLIENT_ID_CONFIG, abortIfMissing = true)
        setKafkaProperty(ConsumerConfig.CHECK_CRCS_CONFIG)
        setKafkaProperty(ConsumerConfig.CLIENT_DNS_LOOKUP_CONFIG)
        setKafkaProperty(ConsumerConfig.CLIENT_RACK_CONFIG)
        setKafkaProperty(ConsumerConfig.CONNECTIONS_MAX_IDLE_MS_CONFIG)
        setKafkaProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true)
        setKafkaProperty(ConsumerConfig.EXCLUDE_INTERNAL_TOPICS_CONFIG)
        setKafkaProperty(ConsumerConfig.FETCH_MAX_BYTES_CONFIG)
        setKafkaProperty(ConsumerConfig.FETCH_MAX_WAIT_MS_CONFIG)
        setKafkaProperty(ConsumerConfig.FETCH_MIN_BYTES_CONFIG)
        setKafkaProperty(ConsumerConfig.GROUP_ID_CONFIG, abortIfMissing = true)
        setKafkaProperty(ConsumerConfig.GROUP_INSTANCE_ID_CONFIG)
        setKafkaProperty(ConsumerConfig.HEARTBEAT_INTERVAL_MS_CONFIG)
        setKafkaProperty(ConsumerConfig.INTERCEPTOR_CLASSES_CONFIG)
        setKafkaProperty(ConsumerConfig.ISOLATION_LEVEL_CONFIG)
        setKafkaProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer::class.java)
        setKafkaProperty(ConsumerConfig.MAX_PARTITION_FETCH_BYTES_CONFIG)
        setKafkaProperty(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG)
        setKafkaProperty(ConsumerConfig.MAX_POLL_RECORDS_CONFIG)
        setKafkaProperty(ConsumerConfig.METADATA_MAX_AGE_CONFIG)
        setKafkaProperty(ConsumerConfig.METRICS_NUM_SAMPLES_CONFIG)
        setKafkaProperty(ConsumerConfig.METRICS_RECORDING_LEVEL_CONFIG)
        setKafkaProperty(ConsumerConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG)
        setKafkaProperty(ConsumerConfig.METRIC_REPORTER_CLASSES_CONFIG)
        setKafkaProperty(ConsumerConfig.PARTITION_ASSIGNMENT_STRATEGY_CONFIG)
        setKafkaProperty(ConsumerConfig.RECEIVE_BUFFER_CONFIG)
        setKafkaProperty(ConsumerConfig.RECONNECT_BACKOFF_MAX_MS_CONFIG)
        setKafkaProperty(ConsumerConfig.RECONNECT_BACKOFF_MS_CONFIG)
        setKafkaProperty(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG)
        setKafkaProperty(ConsumerConfig.RETRY_BACKOFF_MS_CONFIG)
        setKafkaProperty(ConsumerConfig.SEND_BUFFER_CONFIG)
        setKafkaProperty(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG)
        setKafkaProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer::class.java)
        setKafkaProperty(AdminClientConfig.SECURITY_PROTOCOL_CONFIG)
        setKafkaProperty(SslConfigs.SSL_KEYSTORE_TYPE_CONFIG)
        setKafkaProperty(SslConfigs.SSL_KEYSTORE_CERTIFICATE_CHAIN_CONFIG)
        setKafkaProperty(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG)
        setKafkaProperty(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG)
        setKafkaProperty(SslConfigs.SSL_KEY_PASSWORD_CONFIG)
        setKafkaProperty(SslConfigs.SSL_KEYSTORE_KEY_CONFIG)
        setKafkaProperty(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG)
        setKafkaProperty(SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG)
        setKafkaProperty(SslConfigs.SSL_TRUSTSTORE_CERTIFICATES_CONFIG)
    }

    fun stream() {
        setKafkaProperty(StreamsConfig.APPLICATION_ID_CONFIG, abortIfMissing = true)
        setKafkaProperty(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, abortIfMissing = true)
        setKafkaProperty(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().javaClass)
        setKafkaProperty(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().javaClass)
        setKafkaProperty(StreamsConfig.APPLICATION_SERVER_CONFIG)
        setKafkaProperty(StreamsConfig.ADMIN_CLIENT_PREFIX)
        setKafkaProperty(StreamsConfig.AT_LEAST_ONCE)
        setKafkaProperty(StreamsConfig.BUFFERED_RECORDS_PER_PARTITION_CONFIG)
        setKafkaProperty(StreamsConfig.CLIENT_ID_CONFIG)
        setKafkaProperty(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG)
        setKafkaProperty(StreamsConfig.CONNECTIONS_MAX_IDLE_MS_CONFIG)
        setKafkaProperty(StreamsConfig.CONSUMER_PREFIX)
        setKafkaProperty(StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG)
        setKafkaProperty(StreamsConfig.DEFAULT_PRODUCTION_EXCEPTION_HANDLER_CLASS_CONFIG)
        setKafkaProperty(StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG)
        setKafkaProperty(StreamsConfig.GLOBAL_CONSUMER_PREFIX)
        setKafkaProperty(StreamsConfig.METRIC_REPORTER_CLASSES_CONFIG)
        setKafkaProperty(StreamsConfig.METRICS_NUM_SAMPLES_CONFIG)
        setKafkaProperty(StreamsConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG)
        setKafkaProperty(StreamsConfig.MAIN_CONSUMER_PREFIX)
        setKafkaProperty(StreamsConfig.MAX_TASK_IDLE_MS_CONFIG)
        setKafkaProperty(StreamsConfig.METADATA_MAX_AGE_CONFIG)
        setKafkaProperty(StreamsConfig.METRICS_RECORDING_LEVEL_CONFIG)
        setKafkaProperty(StreamsConfig.NO_OPTIMIZATION)
        setKafkaProperty(StreamsConfig.NUM_STANDBY_REPLICAS_CONFIG)
        setKafkaProperty(StreamsConfig.NUM_STREAM_THREADS_CONFIG)
        setKafkaProperty(StreamsConfig.OPTIMIZE)
        setKafkaProperty(StreamsConfig.POLL_MS_CONFIG)
        setKafkaProperty(StreamsConfig.PROCESSING_GUARANTEE_CONFIG)
        setKafkaProperty(StreamsConfig.PRODUCER_PREFIX)
        setKafkaProperty(StreamsConfig.RECEIVE_BUFFER_CONFIG)
        setKafkaProperty(StreamsConfig.RECONNECT_BACKOFF_MAX_MS_CONFIG)
        setKafkaProperty(StreamsConfig.RECONNECT_BACKOFF_MS_CONFIG)
        setKafkaProperty(StreamsConfig.REPLICATION_FACTOR_CONFIG)
        setKafkaProperty(StreamsConfig.REQUEST_TIMEOUT_MS_CONFIG)
        setKafkaProperty(StreamsConfig.RESTORE_CONSUMER_PREFIX)
        setKafkaProperty(StreamsConfig.RETRY_BACKOFF_MS_CONFIG)
        setKafkaProperty(StreamsConfig.ROCKSDB_CONFIG_SETTER_CLASS_CONFIG)
        setKafkaProperty(StreamsConfig.SECURITY_PROTOCOL_CONFIG)
        setKafkaProperty(StreamsConfig.SEND_BUFFER_CONFIG)
        setKafkaProperty(StreamsConfig.STATE_CLEANUP_DELAY_MS_CONFIG)
        setKafkaProperty(StreamsConfig.STATE_DIR_CONFIG)
        setKafkaProperty(StreamsConfig.TOPIC_PREFIX)
        setKafkaProperty(StreamsConfig.WINDOW_STORE_CHANGE_LOG_ADDITIONAL_RETENTION_MS_CONFIG)
        setKafkaProperty(StreamsConfig.producerPrefix(ProducerConfig.MAX_REQUEST_SIZE_CONFIG))
        setKafkaProperty(SslConfigs.SSL_KEYSTORE_TYPE_CONFIG)
        setKafkaProperty(SslConfigs.SSL_KEYSTORE_CERTIFICATE_CHAIN_CONFIG)
        setKafkaProperty(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG)
        setKafkaProperty(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG)
        setKafkaProperty(SslConfigs.SSL_KEY_PASSWORD_CONFIG)
        setKafkaProperty(SslConfigs.SSL_KEYSTORE_KEY_CONFIG)
        setKafkaProperty(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG)
        setKafkaProperty(SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG)
        setKafkaProperty(SslConfigs.SSL_TRUSTSTORE_CERTIFICATES_CONFIG)

    }

    fun producer() {
        setKafkaProperty(ProducerConfig.CLIENT_ID_CONFIG, abortIfMissing = true)
        setKafkaProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, abortIfMissing = true)
        setKafkaProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer::class.java)
        setKafkaProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer::class.java)
        setKafkaProperty(ProducerConfig.BATCH_SIZE_CONFIG, 16384)
        setKafkaProperty(ProducerConfig.BUFFER_MEMORY_CONFIG, 33445532)
        setKafkaProperty(ProducerConfig.LINGER_MS_CONFIG, 1)
        setKafkaProperty(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, true)
        setKafkaProperty(ProducerConfig.ACKS_CONFIG)
        setKafkaProperty(ProducerConfig.COMPRESSION_TYPE_CONFIG, "zstd")
        setKafkaProperty(ProducerConfig.TRANSACTION_TIMEOUT_CONFIG)
        setKafkaProperty(ProducerConfig.TRANSACTIONAL_ID_CONFIG)
        setKafkaProperty(ProducerConfig.CONNECTIONS_MAX_IDLE_MS_CONFIG)
        setKafkaProperty(ProducerConfig.DELIVERY_TIMEOUT_MS_CONFIG)
        setKafkaProperty(ProducerConfig.SEND_BUFFER_CONFIG)
        setKafkaProperty(ProducerConfig.INTERCEPTOR_CLASSES_CONFIG)
        setKafkaProperty(ProducerConfig.RETRY_BACKOFF_MS_CONFIG)
        setKafkaProperty(ProducerConfig.RETRIES_CONFIG)
        setKafkaProperty(ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG)
        setKafkaProperty(ProducerConfig.PARTITIONER_CLASS_CONFIG)
        setKafkaProperty(ProducerConfig.METRIC_REPORTER_CLASSES_CONFIG)
        setKafkaProperty(ProducerConfig.METRICS_NUM_SAMPLES_CONFIG)
        setKafkaProperty(ProducerConfig.METRICS_RECORDING_LEVEL_CONFIG)
        setKafkaProperty(ProducerConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG)
        setKafkaProperty(ProducerConfig.MAX_BLOCK_MS_CONFIG)
        setKafkaProperty(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION)
        setKafkaProperty(ProducerConfig.MAX_REQUEST_SIZE_CONFIG)
        setKafkaProperty(ProducerConfig.METADATA_MAX_AGE_CONFIG)
        setKafkaProperty(ProducerConfig.RECEIVE_BUFFER_CONFIG)
        setKafkaProperty(ProducerConfig.RECONNECT_BACKOFF_MAX_MS_CONFIG)
        setKafkaProperty(ProducerConfig.RECONNECT_BACKOFF_MS_CONFIG)
        setKafkaProperty(AdminClientConfig.SECURITY_PROTOCOL_CONFIG)
        setKafkaProperty(SslConfigs.SSL_KEYSTORE_TYPE_CONFIG)
        setKafkaProperty(SslConfigs.SSL_KEYSTORE_CERTIFICATE_CHAIN_CONFIG)
        setKafkaProperty(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG)
        setKafkaProperty(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG)
        setKafkaProperty(SslConfigs.SSL_KEY_PASSWORD_CONFIG)
        setKafkaProperty(SslConfigs.SSL_KEYSTORE_KEY_CONFIG)
        setKafkaProperty(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG)
        setKafkaProperty(SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG)
        setKafkaProperty(SslConfigs.SSL_TRUSTSTORE_CERTIFICATES_CONFIG)
    }

    private fun setKafkaProperty(propertyName: String, defaultValue: Any? = null, abortIfMissing: Boolean = false) {
        val envProperty = propertyName.replace("\\.".toRegex(), "_").uppercase()
        when {
            System.getenv(envProperty) != null -> {
                log.debug("Found value for property $propertyName in environment variable $envProperty.")
                settings.setProperty(propertyName, System.getenv(envProperty))
            }
            map.containsKey(propertyName) -> {
                log.debug("Found value for property $propertyName in app.yml file.")
                settings.setProperty(propertyName, map[propertyName])
            }
            defaultValue != null -> {
                log.debug("Using default value {} for {}.", defaultValue, propertyName)
                settings[propertyName] = defaultValue
            }
            abortIfMissing -> {
                log.error("Required producer property $propertyName was not set! Aborting...")
                throw MissingSettingException("missing", "kafka.producer.$propertyName")
            }
            else -> log.trace("No value for property $propertyName found")
        }
    }
}
