/*
   Copyright 2020 Jonas Waeber

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package ch.memobase.settings

import java.io.File
import java.io.FileInputStream
import java.util.Optional
import java.util.Properties
import kotlin.system.exitProcess
import org.apache.logging.log4j.LogManager
import org.snakeyaml.engine.v2.api.Load
import org.snakeyaml.engine.v2.api.LoadSettings
import org.snakeyaml.engine.v2.exceptions.MissingEnvironmentVariableException


@Suppress("UNCHECKED_CAST")
class SettingsLoader(
    registeredAppSettings: List<String>,
    private val fileName: String = "app.yml",
    useProducerConfig: Boolean = false,
    useStreamsConfig: Boolean = false,
    useConsumerConfig: Boolean = false,
    readSftpSettings: Boolean = false
) {
    private val log = LogManager.getLogger("SettingsLoader")

    private fun loadYaml(): Any {
        val settings = LoadSettings.builder().setEnvConfig(Optional.of(CustomEnvConfig())).build()
        val load = Load(settings)
        try {
            val file = File("/configs/$fileName")
            return if (file.isFile) {
                load.loadFromInputStream(FileInputStream(file))
            } else {
                log.warn("Loading default properties in $fileName from classpath!")
                load.loadFromInputStream(ClassLoader.getSystemResourceAsStream(fileName))
            }
        } catch (ex: MissingEnvironmentVariableException) {
            throw MissingSettingException("env", ex.localizedMessage)
        }
    }

    private val mappedYaml: Map<String, Any>
    val inputTopic: String
    val outputTopic: String
    val processReportTopic: String
    val appSettings = Properties()
    val sftpSettings: SftpSettings
    val kafkaConsumerSettings = Properties()
    val kafkaProducerSettings = Properties()
    val kafkaStreamsSettings = Properties()
    init {
        try {
            val rawYaml = loadYaml()
            mappedYaml = rawYaml as Map<String, Any>
            if (useConsumerConfig || useStreamsConfig || useProducerConfig) {
                val kafkaOptions = mappedYaml["kafka"] as Map<String, Any>
                val topics = kafkaOptions["topic"] as Map<String, String>
                inputTopic = topics["in"].orEmpty()
                if (inputTopic.isEmpty()) {
                    if (useStreamsConfig || useConsumerConfig) {
                        throw MissingSettingException("missing", "kafka.topic.in")
                    }
                }
                outputTopic = topics["out"].orEmpty()
                if (outputTopic.isEmpty()) {
                    if (useStreamsConfig || useProducerConfig) {
                        throw MissingSettingException("missing", "kafka.topic.out")
                    }
                }

                processReportTopic = topics["process"].orEmpty()
                if (processReportTopic.isEmpty()) {
                    throw MissingSettingException("missing", "kafka.topic.process")
                }

                if (useProducerConfig) {
                    val suppliedKafkaProducerSettings = kafkaOptions["producer"] as Map<String, String>
                    KafkaSettings(kafkaProducerSettings, suppliedKafkaProducerSettings, log).producer()
                }
                if (useStreamsConfig) {
                    val suppliedKafkaStreamsSettings = kafkaOptions["streams"] as Map<String, String>
                    KafkaSettings(kafkaStreamsSettings, suppliedKafkaStreamsSettings, log).stream()
                }
                if (useConsumerConfig) {
                    val suppliedKafkaConsumerSettings = kafkaOptions["consumer"] as Map<String, String>
                    KafkaSettings(kafkaConsumerSettings, suppliedKafkaConsumerSettings, log).consumer()
                }
            } else {
                inputTopic = ""
                outputTopic = ""
                processReportTopic = ""
            }

            registeredAppSettings.forEach {
                appSettings.setProperty(it, addSetting(it))
            }

            sftpSettings = if (readSftpSettings) {
                val sftp = mappedYaml["sftp"] as Map<*, *>
                val port = try {
                    sftp["port"] as Int
                } catch (ex: ClassCastException) {
                    try {
                        (sftp["port"] as String).toInt()
                    } catch (ex: NumberFormatException) {
                        throw InvalidSettingsValue("sftp.port", sftp["port"]!!, "an integer")
                    }
                }
                SftpSettings(sftp["host"] as String, port, sftp["user"] as String, sftp["password"] as String)
            } else {
                SftpSettings("", 0, "", "")
            }
        } catch (ex: ClassCastException) {
            ex.printStackTrace()
            log.error("The properties file has an invalid structure: $ex")
            exitProcess(1)
        } catch (ex: MissingSettingException) {
            log.error(ex.message)
            exitProcess(1)
        }
    }

    private fun addSetting(base: String): String {
        val levels = base.split('.')
        return recursiveFunction(levels.first(), levels.drop(1), mappedYaml["app"] as Map<String, Any>, base)
    }

    private fun recursiveFunction(current: String, rest: List<String>, map: Map<String, Any>, base: String): String {
        if (map.containsKey(current)) {
            return when (val value = map[current]) {
                is String -> value.ifEmpty { throw MissingSettingException("missing", base) }
                is Int -> value.toString()
                is Boolean -> value.toString()
                null -> throw MissingSettingException("missing", base)
                is Map<*, *> -> recursiveFunction(rest.first(), rest.drop(1), value as Map<String, Any>, base)
                else -> throw MissingSettingException("missing", base)
            }
        } else {
            throw MissingSettingException("missing", base)
        }
    }
}
