/*
   Copyright 2020 Jonas Waeber

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package ch.memobase.settings

import org.apache.kafka.common.header.Headers
import org.apache.kafka.streams.processor.api.FixedKeyProcessor
import org.apache.kafka.streams.processor.api.FixedKeyProcessorContext
import org.apache.kafka.streams.processor.api.FixedKeyRecord

class HeaderExtractionTransform<T> : FixedKeyProcessor<String, T, Pair<T, HeaderMetadata>> {

    private var context: FixedKeyProcessorContext<String, Pair<T, HeaderMetadata>>? = null

    override fun init(context: FixedKeyProcessorContext<String, Pair<T, HeaderMetadata>>?) {
        this.context = context
    }

    private fun extract(tag: String, headers: Headers): String? {
        val headerValues = headers.headers(tag)
        return if (headerValues.count() > 0 && headerValues.first().value() != null) {
            headerValues.first().value().toString(Charsets.UTF_8)
        } else
            null
    }

    override fun process(record: FixedKeyRecord<String, T>) {
        context.let { fixedKeyProcessorContext ->
            if (fixedKeyProcessorContext == null) {
                throw IllegalStateException("Processor context is not initialized")
            }

            val headers = record.headers()
            val newRecord = record.withValue(
                Pair(
                    record.value(),
                    HeaderMetadata(
                        extract("recordSetId", headers).let { it ?: "PLACEHOLDER" },
                        extract("sessionId", headers).let { it ?: "PLACEHOLDER" },
                        extract("institutionId", headers).let { it ?: "PLACEHOLDER" },
                        extract("isPublished", headers).let { it?.toBoolean() ?: false },
                        extract("xmlRecordTag", headers).let { it ?: "record" },
                        extract("xmlIdentifierFieldName", headers).let { it ?: "id" },
                        extract("tableSheetIndex", headers).let { it?.toInt() ?: 1 },
                        extract("tableHeaderCount", headers).let { it?.toInt() ?: 1 },
                        extract("tableHeaderIndex", headers).let { it?.toInt() ?: 1 },
                        extract("tableIdentifierIndex", headers).let { it?.toInt() ?: 1 },
                    )
                )
            )

            fixedKeyProcessorContext.forward(
                newRecord
            )
            fixedKeyProcessorContext.commit()
        }
    }

    override fun close() {
        context = null
    }
}
