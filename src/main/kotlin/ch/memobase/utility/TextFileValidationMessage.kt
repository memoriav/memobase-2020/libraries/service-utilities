/*
   Copyright 2020 Jonas Waeber

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package ch.memobase.utility

import kotlinx.serialization.Serializable
import kotlinx.serialization.SerializationException
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.message.ObjectMessage

/**
 * The output structure of the text file validation service. Use this class to parse the message in the
 * output topic of that service.
 */
@Serializable
data class TextFileValidationMessage(
    /**
     * The file format of the validated file.
     */
    val format: AcceptedFileFormat,
    /**
     * Absolute path of the validated file on the sFTP server.
     */
    val path: String
) {
    companion object {
        private val log = LogManager.getLogger(this::class.java)

        /**
         * Parse a list of [TextFileValidationMessage] from a JSON string.
         */
        fun fromJson(data: String): List<TextFileValidationMessage> {
            return try {
                listOf(Json.decodeFromString(data))
            } catch (ex: SerializationException) {
                log.error(ObjectMessage(ex))
                emptyList()
            }
        }
    }

    /**
     * Serialize a [TextFileValidationMessage] to a JSON string.
     */
    fun toJson(): String {
        return Json.encodeToString(this)
    }
}



