/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
plugins {
    `maven-publish`
    `java-library`
    jacoco
    kotlin("jvm") version "1.9.22"
    id("org.jetbrains.kotlin.plugin.serialization") version "1.9.10"
    id("io.freefair.git-version") version "6.2.0"
    id("org.jetbrains.dokka") version "1.9.20"
}

group = "ch.memobase"

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(21)
    }
}

repositories {
    mavenCentral()
}

dependencies {
    // Logging Framework
    // https://central.sonatype.com/artifact/org.apache.logging.log4j/log4j-api
    implementation("org.apache.logging.log4j:log4j-api:2.23.1")

    // https://central.sonatype.com/artifact/org.apache.kafka/kafka-clients
    implementation("org.apache.kafka:kafka-clients:3.8.0")

    // https://central.sonatype.com/artifact/org.apache.kafka/kafka-streams
    implementation("org.apache.kafka:kafka-streams:3.8.0")

    // embedded sftp server
    // https://central.sonatype.com/artifact/com.github.marschall/memoryfilesystem
    implementation("com.github.marschall:memoryfilesystem:2.8.0")
    // https://central.sonatype.com/artifact/org.apache.sshd/sshd-core
    implementation("org.apache.sshd:sshd-core:2.13.1")
    // https://central.sonatype.com/artifact/org.apache.sshd/sshd-sftp
    implementation("org.apache.sshd:sshd-sftp:2.13.1")

    // JSON Parser
    // https://central.sonatype.com/artifact/org.jetbrains.kotlinx/kotlinx-serialization-json
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.3")

    // SFTP Client
    // https://central.sonatype.com/artifact/com.hierynomus/sshj
    implementation("com.hierynomus:sshj:0.38.0")

    // YAML Parser
    // https://central.sonatype.com/artifact/org.snakeyaml/snakeyaml-engine
    implementation("org.snakeyaml:snakeyaml-engine:2.7")

    // Apache Jena
    // https://central.sonatype.com/artifact/org.apache.jena/apache-jena-libs
    implementation("org.apache.jena:apache-jena-libs:5.1.0")

    // JUNIT
    // https://central.sonatype.com/artifact/org.junit.jupiter/junit-jupiter-engine
    implementation("org.junit.jupiter:junit-jupiter-engine:5.11.0")
    // https://central.sonatype.com/artifact/org.junit.jupiter/junit-jupiter-params
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.11.0")

    // https://mvnrepository.com/artifact/org.apache.kafka/kafka-streams-test-utils
    testImplementation("org.apache.kafka:kafka-streams-test-utils:3.7.0")
    // https://central.sonatype.com/artifact/org.assertj/assertj-core
    testImplementation("org.assertj:assertj-core:3.26.0")

    // https://central.sonatype.com/artifact/org.apache.logging.log4j/log4j-core
    testImplementation("org.apache.logging.log4j:log4j-core:2.23.1")
    // https://central.sonatype.com/artifact/org.slf4j/slf4j-nop
    testImplementation("org.slf4j:slf4j-nop:2.0.13")

    // https://central.sonatype.com/artifact/org.elasticsearch/elasticsearch
    testImplementation("co.elastic.clients:elasticsearch-java:8.14.3")

    testImplementation("com.fasterxml.jackson.core:jackson-databind:2.17.0")
}

configurations {
    all {
        exclude(group = "org.slf4j", module = "slf4j-log4j12")
    }
}

publishing {
    publications {
        create<MavenPublication>("memobase-service-utilities") {
            groupId = "ch.memobase"
            artifactId = "memobase-service-utilities"
            from(components["java"])
            pom {
                name = "Memobase Service Utilities"
                description = "A general purpose library which collects many utility functions, classes and constants used in the various memobase services."
                url = "https://gitlab.switch.ch/memoriav/memobase-2020/libraries/service-utilities"
                licenses {
                    license {
                        name = "The Apache License, Version 2.0"
                        url = "https://www.apache.org/licenses/LICENSE-2.0.txt"
                    }

                }
                developers {
                    developer {
                        id = "jonas.waeber"
                        name = "Jonas Waeber"
                        email = "jonas.waeber@unibas.ch"
                    }
                }
                scm {
                    connection = "scm:git:https://gitlab.switch.ch/memoriav/memobase-2020/libraries/service-utilities.git"
                    developerConnection = "scm:git:git@gitlab.switch.ch:memoriav/memobase-2020/libraries/service-utilities.git"
                    url = "https://gitlab.switch.ch/memoriav/memobase-2020/libraries/service-utilities"
                }

            }
        }

        repositories {
            maven {
                url = uri("https://gitlab.switch.ch/api/v4/projects/1324/packages/maven")
                name = "GitLabRepository"
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                }
                authentication {
                    create("header", HttpHeaderAuthentication::class)
                }
            }
        }
    }
}

tasks.named<Test>("test") {
    useJUnitPlatform()

    testLogging {
        setEvents(mutableListOf("passed", "skipped", "failed"))
    }
}

tasks.jacocoTestReport {
    dependsOn(tasks.test)
    reports {
        csv.required = true
    }
}
